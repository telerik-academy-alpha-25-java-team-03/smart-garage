package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.smartgarage.Helpers.createMockEmployee;
import static com.telerikacademy.web.smartgarage.Helpers.createMockManufacturer;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ManufacturerServiceImplTests {

    @Mock
    ManufacturerRepository mockRepository;

    @InjectMocks
    ManufacturerServiceImpl mockService;

    static User employee;
    static Manufacturer manufacturer;

    @BeforeAll
    static void before() {
        manufacturer = createMockManufacturer();
        employee = createMockEmployee();
    }

    @Test
    public void getAll_Should_ReturnManufacturer_When_MatchExists() {
        // Arrange
        List<Manufacturer> manufacturers = new ArrayList<>();
        manufacturers.add(createMockManufacturer());
        manufacturers.add(createMockManufacturer());
        when(mockRepository.getAll())
                .thenReturn(manufacturers);
        // Act
        List<Manufacturer> result = mockService.getAll(employee);
        // Assert
        assertEquals(manufacturers, result);
    }

    @Test
    public void getById_Should_ReturnManufacturer_When_MatchExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(manufacturer);
        // Act
        Manufacturer result = mockService.getById(employee, 1);
        // Assert
        assertEquals(manufacturer, result);
    }

    @Test
    public void getByField_Should_ReturnManufacturer_When_MatchExists() {
        // Arrange
        when(mockRepository.getByField("MockManufacturer"))
                .thenReturn(manufacturer);
        // Act
        Manufacturer result = mockService.getByField(employee, "MockManufacturer");
        // Assert
        assertEquals(manufacturer, result);
    }

    @Test
    public void create_Should_CallRepository_When_ManufacturerWithSameNameDoesNotExists() {
        // Arrange
        when(mockRepository.getByField(manufacturer.getName()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        mockService.create(employee, manufacturer);
        // Assert
        verify(mockRepository, times(1))
                .create(Mockito.any(Manufacturer.class));
    }

    @Test
    public void create_ShouldNot_CallRepository_When_ManufacturerWithSameNameExists() {
        // Arrange
        when(mockRepository.getByField(manufacturer.getName()))
                .thenReturn(manufacturer);
        // Act
        mockService.create(employee, manufacturer);
        // Assert
        verify(mockRepository, times(0))
                .create((any(Manufacturer.class)));

    }

}
