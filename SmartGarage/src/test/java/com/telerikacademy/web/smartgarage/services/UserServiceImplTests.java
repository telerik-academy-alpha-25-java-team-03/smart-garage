package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.web.smartgarage.utilities.MailSender;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.smartgarage.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl mockService;

    static User customer;
    static User employee;
    static User admin;
    static JavaMailSender sender;
    static MailSender mockMailSender;
    static List<User> mockList;
    static UserFilterParameters mockUfp;

    @BeforeAll
    static void before() {
        customer = createMockCustomer();
        employee = createMockEmployee();
        admin = createMockAdmin();
        mockUfp = createMockUfp();
        mockMailSender = new MailSender(sender);
        mockList = new ArrayList<>();
        mockList.add(customer);
        mockList.add(employee);
        mockList.add(admin);
    }

    @Test
    public void getAll_Should_ReturnUser_When_MatchExists() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(mockList);
        // Act
        List<User> result = mockService.getAll(admin);
        // Assert
        assertEquals(mockList, result);
    }

    @Test
    public void getById_Should_ReturnUser_When_MatchExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(customer);
        // Act
        User result = mockService.getById(admin, 1);
        // Assert
        assertEquals(customer, result);
    }

    @Test
    public void getByField_Should_ReturnUser_When_MatchExists() {
        // Arrange
        when(mockRepository.getByField("MockFirstName"))
                .thenReturn(customer);
        // Act
        User result = mockService.getByField(admin, "MockFirstName");
        // Assert
        assertEquals(customer, result);
    }

    @Test
    public void getByEmail_Should_ReturnUser_When_MatchExists() {
        // Arrange
        when(mockRepository.getByEmail("mockcustomer@gmail.com"))
                .thenReturn(customer);
        // Act
        User result = mockService.getByEmail("mockcustomer@gmail.com");
        // Assert
        assertEquals(customer, result);
    }

    @Test
    public void create_Should_Throw_When_UserWithSameEmailExists() {
        // Arrange
        when(mockRepository.getByEmail(customer.getEmail()))
                .thenReturn(customer);
        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> mockService.create(admin, customer));

    }

    @Test
    public void update_Should_Throw_When_UserWithSameEmailExists() {
        // Arrange
        var customerToUpdate = createMockCustomer();
        customer.setId(customerToUpdate.getId() + 1);
        when(mockRepository.getByEmail(customerToUpdate.getEmail()))
                .thenReturn(customer);
        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> mockService.update(admin, customerToUpdate));
    }

    @Test
    public void update_Should_CallRepository_When_UserWithSameEmailDoesNotExists() {
        // Arrange
        when(mockRepository.getByEmail(customer.getEmail()))
                .thenThrow(new EntityNotFoundException(
                        "User", "Email", customer.getEmail()));
        // Act
        mockService.update(admin, customer);
        // Assert
        verify(mockRepository, times(1))
                .update(any(User.class));
    }

    @Test
    public void update_Should_CallRepository_When_UserWithSameIdDoesNotExists() {
        // Arrange
        when(mockRepository.getByEmail(customer.getEmail()))
                .thenReturn(customer);
        // Act
        mockService.update(admin, customer);
        // Assert
        verify(mockRepository, times(1))
                .update(any(User.class));
    }

    @Test
    public void delete_Should_Throw_When_UserWithSameIdDoesNotExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenThrow(EntityNotFoundException.class);
        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> mockService.delete(admin, 1));
    }

    @Test
    public void delete_Should_CallRepository_When_UserWithSameIdExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(customer);
        // Act
        mockService.delete(admin, 1);
        // Assert
        verify(mockRepository, times(1))
                .update(any(User.class));
    }

    @Test
    public void filter_Should_ReturnUsers_When_MatchExists() {
        // Arrange
        when(mockRepository.filter(mockUfp))
                .thenReturn(mockList);
        // Act
        List<User> filterResult = mockService.filter(admin, mockUfp);
        // Assert
        assertEquals(mockList, filterResult);
    }

    @Test
    public void sort_Should_ReturnUsers_When_MatchExists() {
        Optional<String> firstName = Optional.of("asc");
        Optional<String> beginDate = Optional.of("desc");
        Optional<String> endDate = Optional.of("asc");
        // Arrange
        when(mockRepository.sort(firstName, beginDate, endDate))
                .thenReturn(mockList);
        // Act
        List<User> sortedResult = mockService.sort(admin, firstName, beginDate, endDate);
        // Assert
        assertEquals(mockList, sortedResult);
    }
}
