package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.InvalidRequestParamsException;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.smartgarage.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    @Mock
    VisitRepository mockRepository;

    @InjectMocks
    VisitServiceImpl mockService;

    static User employee;
    static Visit visit;
    static List<Visit> mockList;
    static VisitFilterParameters vfp;

    @BeforeAll
    static void before() {
        employee = createMockEmployee();
        visit = createMockVisit();
        mockList = new ArrayList<>();
        vfp = createMockVisitFilterParameters();
    }

    @Test
    public void getAll_Should_ReturnVisits_When_MatchExists() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(mockList);
        // Act
        List<Visit> result = mockService.getAll(employee);
        // Assert
        assertEquals(mockList, result);
    }

    @Test
    public void getById_Should_ReturnVisit_When_MatchExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(visit);
        // Act
        Visit result = mockService.getById(employee, 1);
        // Assert
        assertEquals(visit, result);
    }

    @Test
    public void create_Should_CallRepository_When_ParametersAreValid() {
        // Act
        mockService.create(employee, visit);
        // Assert
        verify(mockRepository, times(1))
                .create(any(Visit.class));
    }

    @Test
    public void update_Should_CallRepository_When_ParametersAreValid() {
        // Act
        mockService.update(employee, visit);
        // Assert
        verify(mockRepository, times(1))
                .update(any(Visit.class));
    }

    @Test
    public void delete_Should_Throw_When_VisitWithSameIdDoesNotExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenThrow(EntityNotFoundException.class);
        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> mockService.delete(employee, 1));
    }

    @Test
    public void delete_Should_CallRepository_When_VisitWithSameIdExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(visit);
        // Act
        mockService.delete(employee, 1);
        // Assert
        verify(mockRepository, times(1))
                .update(any(Visit.class));
    }

    @Test
    public void filter_Should_ReturnVisits_When_MatchExists() {
        // Arrange
        when(mockRepository.filter(mockService.getRate(Optional.of("EUR")), vfp))
                .thenReturn(mockList);
        // Act
        List<Visit> result = mockService.filter(employee, Optional.of("EUR"), vfp);
        // Assert
        assertEquals(mockList, result);
    }

    @Test
    public void sort_Should_ReturnVisits_When_MatchExists() {
        // Arrange
        when(mockRepository.sort(Optional.of("asc"), Optional.of("desc")))
                .thenReturn(mockList);
        // Act
        List<Visit> result = mockService.sort(employee, Optional.of("asc"), Optional.of("desc"));
        // Assert
        assertEquals(mockList, result);
    }

    @Test
    public void sort_Should_Throw_When_ParametersAreInvalid() {
        // Act, Assert
        assertThrows(InvalidRequestParamsException.class,
                () -> mockService.sort(employee, Optional.of("2021-03-01"), Optional.of("2021-05-01")));
    }

}
