package com.telerikacademy.web.smartgarage;

import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.filterParameters.OfferedServiceFilterParameters;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;
import com.telerikacademy.web.smartgarage.models.filterParameters.VehicleFilterParameters;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class Helpers {

    public static Manufacturer createMockManufacturer() {
        var mockManufacturer = new Manufacturer();
        mockManufacturer.setId(1);
        mockManufacturer.setName("MockManufacturer");
        mockManufacturer.setEnabled(true);
        return mockManufacturer;
    }

    public static Model createMockModel() {
        var mockModel = new Model();
        mockModel.setId(1);
        mockModel.setName("MockModel");
        mockModel.setManufacturer(createMockManufacturer());
        mockModel.setEnabled(true);
        return mockModel;
    }

    public static OfferedService createMockOfferedService() {
        var mockOfferedService = new OfferedService();
        mockOfferedService.setId(1);
        mockOfferedService.setName("MockOfferedService");
        mockOfferedService.setPrice(350);
        mockOfferedService.setEnabled(true);
        return mockOfferedService;
    }


    public static OfferedServiceFilterParameters createMockOfp() {
        var ofp = new OfferedServiceFilterParameters();
        ofp.setName(Optional.of("MockOfferedService"));
        ofp.setMinPrice(Optional.of(300.0));
        ofp.setMaxPrice(Optional.of(400.0));
        return ofp;
    }

    public static PasswordResetToken createMockPasswordResetToken() {
        var mockToken = new PasswordResetToken();
        mockToken.setId(1);
        mockToken.setToken();
        mockToken.setExpiryTime();
        mockToken.setAccessed(false);
        return mockToken;
    }

    public static User createMockCustomer() {
        var mockCustomer = new User();
        mockCustomer.setId(1);
        mockCustomer.setFirstName("MockFirstName");
        mockCustomer.setLastName("MockLastName");
        mockCustomer.setEmail("mockcustomer@gmail.com");
        mockCustomer.setPassword("MockPassword");
        mockCustomer.setPhoneNumber("0888776633");
        mockCustomer.setEnabled(true);
        makeCustomer(mockCustomer);
        return mockCustomer;
    }

    public static User createMockEmployee() {
        var mockEmployee = new User();
        mockEmployee.setId(2);
        mockEmployee.setFirstName("MockFirstName");
        mockEmployee.setLastName("MockLastName");
        mockEmployee.setEmail("mockemployee@gmail.com");
        mockEmployee.setPassword("MockPassword");
        mockEmployee.setPhoneNumber("0889662211");
        mockEmployee.setEnabled(true);
        makeEmployee(mockEmployee);
        return mockEmployee;
    }

    public static User createMockAdmin() {
        var mockAdmin = new User();
        mockAdmin.setId(3);
        mockAdmin.setFirstName("MockFirstName");
        mockAdmin.setLastName("MockLastName");
        mockAdmin.setEmail("mockadmin@gmail.com");
        mockAdmin.setPassword("MockPassword");
        mockAdmin.setPhoneNumber("0885556644");
        mockAdmin.setEnabled(true);
        makeAdmin(mockAdmin);
        return mockAdmin;
    }

    public static UserFilterParameters createMockUfp() {
        var ufp = new UserFilterParameters();
        ufp.setFirstName(Optional.of("MockFirstName"));
        ufp.setLastName(Optional.of("MockLastName"));
        ufp.setEmail(Optional.of("mockcustomer@gmail.com"));
        ufp.setVehicleModelId(Optional.of(1));
        ufp.setPhoneNumber(Optional.of("0888776633"));
        ufp.setBeginDate(Optional.of("2021-04-15"));
        ufp.setEndDate(Optional.of("2021-04-25"));
        return ufp;
    }

    public static Vehicle createMockVehicle() {
        var mockVehicle = new Vehicle();
        mockVehicle.setId(1);
        mockVehicle.setModel(createMockModel());
        mockVehicle.setRegistrationPlate("PB7799CA");
        mockVehicle.setVin("CBA7856WGB89746RW");
        mockVehicle.setYear(2021);
        mockVehicle.setUser(createMockCustomer());
        mockVehicle.setEnabled(true);
        return mockVehicle;
    }

    public static VehicleFilterParameters createMockVfp() {
        var vfp = new VehicleFilterParameters();
        vfp.setFirstName(Optional.of("MockFirstName"));
        vfp.setLastName(Optional.of("MockLastName"));
        vfp.setEmail(Optional.of("mockcustomer@gmail.com"));
        return vfp;
    }

    public static VisitFilterParameters createMockVisitFilterParameters() {
        var vfp = new VisitFilterParameters();
        vfp.setUserId(1);
        vfp.setVehicleId(Optional.of(1));
        vfp.setRegistrationPlate(Optional.of("PB7799CA"));
        vfp.setBeginDate(Optional.of("2021-03-01"));
        vfp.setEndDate(Optional.of("2021-05-01"));
        return vfp;
    }

    public static Visit createMockVisit() {
        var mockVisit = new Visit();
        mockVisit.setId(1);
        mockVisit.setVehicle(createMockVehicle());
        mockVisit.setBeginDate(createBeginDate());
        mockVisit.setEndDate(createEndDate());
        mockVisit.setTotalPrice(400);
        mockVisit.setEnabled(true);
        return mockVisit;
    }

    public static void makeCustomer(User user) {
        Set<Role> mockRoles = new HashSet<>();
        mockRoles.add(generateCustomerRole());
        user.setRoles(mockRoles);
    }

    public static void makeEmployee(User user) {
        Set<Role> mockRoles = new HashSet<>();
        mockRoles.add(generateEmployeeRole());
        user.setRoles(mockRoles);
    }

    public static void makeAdmin(User user) {
        Set<Role> mockRoles = new HashSet<>();
        mockRoles.add(generateAdminRole());
        user.setRoles(mockRoles);
    }

    public static Role generateCustomerRole() {
        var mockCustomerRole = new Role();
        mockCustomerRole.setId(1);
        mockCustomerRole.setType("Customer");
        return mockCustomerRole;
    }

    public static Role generateEmployeeRole() {
        var mockEmployeeRole = new Role();
        mockEmployeeRole.setId(1);
        mockEmployeeRole.setType("Employee");
        return mockEmployeeRole;
    }

    public static Role generateAdminRole() {
        var mockAdminRole = new Role();
        mockAdminRole.setId(1);
        mockAdminRole.setType("Admin");
        return mockAdminRole;
    }

    public static LocalDate createBeginDate() {
        return LocalDate.of(2021, Month.APRIL, 15);
    }

    public static LocalDate createEndDate() {
        return LocalDate.of(2021, Month.APRIL, 25);
    }

}
