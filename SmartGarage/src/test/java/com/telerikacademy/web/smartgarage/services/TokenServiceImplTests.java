package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.models.PasswordResetToken;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.TokenRepository;
import com.telerikacademy.web.smartgarage.utilities.MailSender;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.smartgarage.Helpers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TokenServiceImplTests {

    @Mock
    TokenRepository mockRepository;

    @InjectMocks
    TokenServiceImpl mockService;

    static User customer;
    static User employee;
    static User admin;
    static JavaMailSender sender;
    static MailSender mockMailSender;
    static List<User> mockList;
    static UserFilterParameters mockUfp;

    @BeforeAll
    static void before() {
        customer = createMockCustomer();
        employee = createMockEmployee();
        admin = createMockAdmin();
        mockUfp = createMockUfp();
        mockMailSender = new MailSender(sender);
        mockList = new ArrayList<>();
        mockList.add(customer);
        mockList.add(employee);
        mockList.add(admin);
    }

    @Test
    public void createToken_Should_CallRepository_When_Invoked() {
        // Arrange
        when(mockRepository.createToken())
                .thenReturn(new PasswordResetToken());
        // Act
        mockService.createToken();
        // Assert
        verify(mockRepository, times(1))
                .createToken();
    }

    @Test
    public void isExpired_Should_CallRepository_When_Invoked() {
        // Arrange
        when(mockRepository.isExpired("tokenText"))
                .thenReturn(true);
        // Act
        mockService.isExpired("tokenText");
        // Assert
        verify(mockRepository, times(1))
                .isExpired(any(String.class));
    }

    @Test
    public void getToken_Should_CallRepository_When_Invoked() {
        // Arrange
        when(mockRepository.getToken("tokenText"))
                .thenReturn(new PasswordResetToken());
        // Act
        mockService.getToken("tokenText");
        // Assert
        verify(mockRepository, times(1))
                .getToken(any(String.class));
    }

}