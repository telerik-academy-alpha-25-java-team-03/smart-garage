package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.filterParameters.VehicleFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.smartgarage.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceImplTests {

    @Mock
    VehicleRepository mockRepository;

    @InjectMocks
    VehicleServiceImpl mockService;

    static User employee;
    static Vehicle vehicle;
    static List<Vehicle> mockList;
    static VehicleFilterParameters mockVfp;

    @BeforeAll
    static void before() {
        employee = createMockEmployee();
        vehicle = createMockVehicle();
        mockList = new ArrayList<>();
        mockVfp = createMockVfp();
    }

    @Test
    public void getAll_Should_ReturnVehicles_When_MatchExists() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(mockList);
        // Act
        List<Vehicle> result = mockService.getAll(employee);
        // Assert
        assertEquals(mockList, result);
    }

    @Test
    public void getById_Should_ReturnVehicle_When_MatchExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(vehicle);
        // Act
        Vehicle result = mockService.getById(employee, 1);
        // Assert
        assertEquals(vehicle, result);
    }

    @Test
    public void getByField_Should_ReturnVehicle_When_MatchExists() {
        // Arrange
        when(mockRepository.getByField("MockVehicle"))
                .thenReturn(vehicle);
        // Act
        Vehicle result = mockService.getByField(employee, "MockVehicle");
        // Assert
        assertEquals(vehicle, result);
    }

    @Test
    public void getByVin_Should_ReturnVehicle_When_MatchExists() {
        // Arrange
        when(mockRepository.getByVin("CBA7856WGB89746RW"))
                .thenReturn(vehicle);
        // Act
        Vehicle result = mockService.getByVin(employee, "CBA7856WGB89746RW");
        // Assert
        assertEquals(vehicle, result);
    }

    @Test
    public void create_Should_CallRepository_When_VehicleWithSameRegistrationPlateAndVinDoesNotExists() {
        // Arrange
        when(mockRepository.getByField(vehicle.getRegistrationPlate()))
                .thenThrow(new EntityNotFoundException("error1"));
        when(mockRepository.getByVin(vehicle.getVin()))
                .thenThrow(new EntityNotFoundException("error1"));
        // Act
        mockService.create(employee, vehicle);
        // Assert
        verify(mockRepository, times(1))
                .create(any(Vehicle.class));
    }

    @Test
    public void create_Should_Throw_When_VehicleWithSameRegistrationPlateNameExists() {
        // Arrange
        when(mockRepository.getByField(vehicle.getRegistrationPlate()))
                .thenReturn(vehicle);
        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> mockService.create(employee, vehicle));

    }

    @Test
    public void update_Should_Throw_When_VehicleWithSameRegistrationPlateNameExists() {
        // Arrange
        var vehicleToUpdate = createMockVehicle();
        vehicle.setId(vehicleToUpdate.getId() + 1);
        when(mockRepository.getByField(vehicleToUpdate.getRegistrationPlate()))
                .thenReturn(vehicle);
        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> mockService.update(employee, vehicleToUpdate));
    }

    @Test
    public void update_Should_CallRepository_When_VehicleWithSameRegistrationPlateDoesNotExists() {
        // Arrange
        when(mockRepository.getByField(vehicle.getRegistrationPlate()))
                .thenThrow(new EntityNotFoundException(
                        "OfferedService", "Registration plate", vehicle.getRegistrationPlate()));
        // Act
        mockService.update(employee, vehicle);
        // Assert
        verify(mockRepository, times(1))
                .update(any(Vehicle.class));
    }

    @Test
    public void update_Should_CallRepository_When_VehicleWithSameIdDoesNotExists() {
        // Arrange
        when(mockRepository.getByField(vehicle.getRegistrationPlate()))
                .thenReturn(vehicle);
        // Act
        mockService.update(employee, vehicle);
        // Assert
        verify(mockRepository, times(1))
                .update(any(Vehicle.class));
    }

    @Test
    public void delete_Should_Throw_When_VehicleWithSameIdDoesNotExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenThrow(EntityNotFoundException.class);
        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> mockService.delete(employee, 1));
    }

    @Test
    public void delete_Should_CallRepository_When_OfferedServiceWithSameIdExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(vehicle);
        // Act
        mockService.delete(employee, 1);
        // Assert
        verify(mockRepository, times(1))
                .update(any(Vehicle.class));
    }

    @Test
    public void delete_Should_CallRepository_When_VehicleWithSameIdDoesNotExists() {
        var user = new User();
        assertThrows(UnauthorizedOperationException.class,
                () -> mockService.delete(user, vehicle.getId()));
    }

    @Test
    public void filterByOwner_Should_ReturnVehicles_When_MatchExists() {
        // Arrange
        vehicle.setUser(createMockCustomer());
        when(mockRepository.filterByOwner(mockVfp))
                .thenReturn(mockList);
        // Act
        List<Vehicle> filterResult = mockService.filterByOwner(employee, mockVfp);
        // Assert
        assertEquals(mockList, filterResult);
    }
}
