package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.filterParameters.OfferedServiceFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.OfferedServiceRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.smartgarage.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OfferedServiceImplTests {

    @Mock
    OfferedServiceRepository mockRepository;

    @InjectMocks
    OfferedServiceServiceImpl mockService;

    static User employee;
    static OfferedService offeredService;
    static List<OfferedService> mockList;
    static OfferedServiceFilterParameters mockFilterParameters;

    @BeforeAll
    static void before() {
        offeredService = createMockOfferedService();
        employee = createMockEmployee();
        mockList = new ArrayList<>();
        mockList.add(offeredService);
        mockFilterParameters = createMockOfp();
    }

    @Test
    public void getAll_Should_ReturnOfferedService_When_MatchExists() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(mockList);
        // Act
        List<OfferedService> result = mockService.getAll(employee);
        // Assert
        assertEquals(mockList, result);
    }

    @Test
    public void getById_Should_ReturnOfferedService_When_MatchExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(offeredService);
        // Act
        OfferedService result = mockService.getById(employee, 1);
        // Assert
        assertEquals(offeredService, result);
    }

    @Test
    public void getByField_Should_ReturnOfferedService_When_MatchExists() {
        // Arrange
        when(mockRepository.getByField("MockOfferedService"))
                .thenReturn(offeredService);
        // Act
        OfferedService result = mockService.getByField(employee, "MockOfferedService");
        // Assert
        assertEquals(offeredService, result);
    }

    @Test
    public void create_Should_CallRepository_When_OfferedServiceWithSameNameDoesNotExists() {
        // Arrange
        when(mockRepository.getByField(offeredService.getName()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        mockService.create(employee, offeredService);
        // Assert
        verify(mockRepository, times(1))
                .create(any(OfferedService.class));
    }

    @Test
    public void create_Should_Throw_When_OfferedServiceWithSameNameExists() {
        // Arrange
        when(mockRepository.getByField(offeredService.getName()))
                .thenReturn(offeredService);
        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> mockService.create(employee, offeredService));

    }

    @Test
    public void update_Should_Throw_When_OfferedServiceWithSameNameExists() {
        // Arrange
        var offeredServiceToUpdate = createMockOfferedService();
        offeredService.setId(offeredServiceToUpdate.getId() + 1);
        when(mockRepository.getByField(offeredServiceToUpdate.getName()))
                .thenReturn(offeredService);
        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> mockService.update(employee, offeredServiceToUpdate));
    }

    @Test
    public void update_Should_CallRepository_When_OfferedServiceWithSameNameDoesNotExists() {
        // Arrange
        when(mockRepository.getByField(offeredService.getName()))
                .thenThrow(new EntityNotFoundException("OfferedService", "name", offeredService.getName()));
        // Act
        mockService.update(employee, offeredService);
        // Assert
        verify(mockRepository, times(1))
                .update(any(OfferedService.class));
    }

    @Test
    public void update_Should_CallRepository_When_OfferedServiceWithSameIdDoesNotExists() {
        // Arrange
        when(mockRepository.getByField(offeredService.getName()))
                .thenReturn(offeredService);
        // Act
        mockService.update(employee, offeredService);
        // Assert
        verify(mockRepository, times(1))
                .update(any(OfferedService.class));
    }

    @Test
    public void delete_Should_Throw_When_OfferedServiceWithSameIdDoesNotExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenThrow(EntityNotFoundException.class);
        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> mockService.delete(employee, 1));
    }

    @Test
    public void delete_Should_CallRepository_When_OfferedServiceWithSameIdExists() {
        // Arrange
        when(mockRepository.getById(1))
                .thenReturn(offeredService);
        // Act
        mockService.delete(employee, 1);
        // Assert
        verify(mockRepository, times(1))
                .update(any(OfferedService.class));
    }

    @Test
    public void filter_Should_ReturnOfferedServices_When_MatchExists() {
        // Arrange
        when(mockRepository.filter(mockFilterParameters))
                .thenReturn(mockList);
        // Act
        List<OfferedService> filterResult = mockService.filter(employee, mockFilterParameters);
        // Assert
        assertEquals(mockList, filterResult);
    }

}
