package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.smartgarage.Helpers.createMockEmployee;
import static com.telerikacademy.web.smartgarage.Helpers.createMockModel;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ModelServiceImplTests {

    @Mock
    ModelRepository mockRepository;

    @InjectMocks
    ModelServiceImpl mockService;

    static User employee;
    static Model model;

    @BeforeAll
    static void before() {
        employee = createMockEmployee();
        model = createMockModel();
    }

    @Test
    public void getAll_Should_ReturnModels_When_MatchExists() {
        // Arrange
        List<Model> models = new ArrayList<>();
        models.add(createMockModel());
        models.add(createMockModel());
        when(mockRepository.getAll())
                .thenReturn(models);
        // Act
        List<Model> result = mockService.getAll(employee);
        // Assert
        assertEquals(models, result);
    }

    @Test
    public void getById_Should_ReturnModel_When_MatchExists() {
        // Arrange
        Model model = createMockModel();
        when(mockRepository.getById(1))
                .thenReturn(model);
        // Act
        Model result = mockService.getById(employee, 1);
        // Assert
        assertEquals(model, result);
    }

    @Test
    public void getByField_Should_ReturnModel_When_MatchExists() {
        // Arrange
        Model model = createMockModel();
        when(mockRepository.getByField("MockModel"))
                .thenReturn(model);
        // Act
        Model result = mockService.getByField(employee, "MockModel");
        // Assert
        assertEquals(model, result);
    }

    @Test
    public void create_Should_CallRepository_When_ModelWithSameNameDoesNotExists() {
        // Arrange
        when(mockRepository.getByField("MockModel"))
                .thenThrow(EntityNotFoundException.class);
        // Act
        mockService.create(employee, model);
        // Assert
        verify(mockRepository, times(1))
                .create(any(Model.class));
    }

    @Test
    public void create_ShouldNot_CallRepository_When_ModelWithSameNameExists() {
        // Arrange
        when(mockRepository.getByField("MockModel"))
                .thenReturn(model);
        // Act
        mockService.create(employee, model);
        // Assert
        verify(mockRepository, times(0))
                .create(any(Model.class));
    }

}
