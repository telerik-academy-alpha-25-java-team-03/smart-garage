package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Manufacturer;

public interface ManufacturerRepository extends ReadRepositoryOperations<Manufacturer> {

    void create(Manufacturer manufacturer);

}
