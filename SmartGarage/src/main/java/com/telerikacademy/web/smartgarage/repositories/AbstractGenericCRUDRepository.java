package com.telerikacademy.web.smartgarage.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class AbstractGenericCRUDRepository<T> extends AbstractGenericGetRepository<T> {

    private final SessionFactory sessionFactory;

    public AbstractGenericCRUDRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    public void create(T objectToCreate) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(objectToCreate);
            session.getTransaction().commit();
        }
    }

    public void update(T objectToUpdate) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(objectToUpdate);
            session.getTransaction().commit();
        }
    }

}
