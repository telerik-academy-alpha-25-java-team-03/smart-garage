package com.telerikacademy.web.smartgarage.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.web.smartgarage.models.enums.VisitStatus;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "begin_date")
    private LocalDate beginDate;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "status")
    private String status;

    @Column(name = "total_price")
    private double totalPrice;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @JsonIgnore
    @Column(name = "enabled")
    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "visits_services",
            joinColumns = @JoinColumn(name = "visit_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id")
    )
    private Set<OfferedService> offeredServices;

    public Visit() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getVisitStatus() {
        return status;
    }

    public void setVisitStatus(String status) {
        this.status = status;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<OfferedService> getOfferedServices() {
        return offeredServices;
    }

    public void setOfferedServices(Set<OfferedService> offeredServices) {
        this.offeredServices = offeredServices;
    }

    public String setStatus() {
        if (LocalDate.now().compareTo(beginDate) < 0) {
            setVisitStatus(VisitStatus.NOT_STARTED.toString());
        } else if (LocalDate.now().compareTo(endDate) > 0) {
            setVisitStatus(VisitStatus.READY_FOR_PICKUP.toString());
        } else {
            setVisitStatus(VisitStatus.IN_PROGRESS.toString());
        }
        return getVisitStatus();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visit visit = (Visit) o;
        return getId() == visit.getId() && isEnabled() == visit.isEnabled() && getBeginDate().equals(visit.getBeginDate()) && getEndDate().equals(visit.getEndDate()) && getVehicle().equals(visit.getVehicle()) && Objects.equals(getOfferedServices(), visit.getOfferedServices());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBeginDate(), getEndDate(), getVehicle());
    }

}
