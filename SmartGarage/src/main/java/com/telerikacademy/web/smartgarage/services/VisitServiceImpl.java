package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.enums.UserRoles;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import com.telerikacademy.web.smartgarage.utilities.CurrencyExchanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static com.telerikacademy.web.smartgarage.utilities.AuthorizationHelper.verifyUserHasRoles;
import static com.telerikacademy.web.smartgarage.utilities.ValidationHelper.verifyIsLegal;

@Service
public class VisitServiceImpl implements VisitService {

    private final VisitRepository visitRepository;
    private final UserRepository userRepository;
    private final VehicleRepository vehicleRepository;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository, UserRepository userRepository, VehicleRepository vehicleRepository) {
        this.visitRepository = visitRepository;
        this.userRepository = userRepository;
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Visit> getAll(User user) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return visitRepository.getAll();
    }

    @Override
    public Visit getById(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return visitRepository.getById(id);
    }

    @Override
    public void create(User user, Visit visit) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        visitRepository.create(visit);
    }

    @Override
    public void update(User user, Visit visit) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        visitRepository.update(visit);
    }

    @Override
    public void delete(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        Visit existing = visitRepository.getById(id);
        existing.setEnabled(false);
        visitRepository.update(existing);
    }

    @Override
    public List<Visit> filter(User user, Optional<String> currency, VisitFilterParameters vfp) {
        verifyUserHasRoles(user, UserRoles.CUSTOMER, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        if (user.hasRights()) {
            vfp.setUserId(null);
        }
        double rate = getRate(currency);
        return visitRepository.filter(rate, vfp);
    }

    @Override
    public List<Visit> sort(User user, Optional<String> beginDate, Optional<String> endDate) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        verifyIsLegal(beginDate);
        verifyIsLegal(endDate);
        return visitRepository.sort(beginDate, endDate);
    }

    protected double getRate(Optional<String> currency) {
        if (currency.isPresent()) {
            CurrencyExchanger currencyExchanger = new CurrencyExchanger();
            return currencyExchanger.getRate(currency.get().toUpperCase(Locale.ROOT));
        } else return 1;
    }

}
