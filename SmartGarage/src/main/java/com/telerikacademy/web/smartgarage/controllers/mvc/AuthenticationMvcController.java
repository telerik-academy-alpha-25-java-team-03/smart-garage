package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.ExpiryTokenFailureException;
import com.telerikacademy.web.smartgarage.models.dtos.ForgotPasswordDto;
import com.telerikacademy.web.smartgarage.models.dtos.LoginDto;
import com.telerikacademy.web.smartgarage.models.dtos.ResetPasswordTokenDto;
import com.telerikacademy.web.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping
public class AuthenticationMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AuthenticationMvcController(UserService userService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDto", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto") LoginDto dto,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        try {
            authenticationHelper.verifyAuthentication(dto.getEmail(), dto.getPassword());
            session.setAttribute("currentUser", dto.getEmail());
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return "login";
        } catch (EntityNotFoundException en) {
            return "not-found";
        }
        return "redirect:/";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/forgot-password")
    public String showForgotPassword(Model model) {
        model.addAttribute("forgotPasswordDto", new ForgotPasswordDto());
        return "forgot-password-form";
    }

    @PostMapping("/forgot-password")
    public String handleForgotPassword(@Valid @ModelAttribute("forgotPasswordDto") ForgotPasswordDto dto,
                                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "forgot-password-form";
        }
        try {
            authenticationHelper.verifyAuthentication(dto.getEmail());
            userService.resetPassword(dto.getEmail());
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return "forgot-password-form";
        }
        return "redirect:/reset-password";
    }

    @GetMapping("/reset-password")
    public String showResetPassword(Model model) {
        model.addAttribute("resetPasswordDto", new ResetPasswordTokenDto());
        return "reset-password-form";
    }

    @PostMapping("/reset-password")
    public String handleResetPassword(@Valid @ModelAttribute("resetPasswordDto") ResetPasswordTokenDto dto,
                                      BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "reset-password-form";
        }
        try {
            authenticationHelper.verifyAuthentication(dto.getEmail());
            userService.resetPasswordConfirm(dto.getToken(), dto.getEmail(), dto.getPassword(), dto.getConfirmPassword());
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return "reset-password-form";
        } catch (ExpiryTokenFailureException e) {
            bindingResult.rejectValue("token", "auth_error", e.getMessage());
            return "forgot-password-form";
        }
        return "redirect:/login";
    }
}
