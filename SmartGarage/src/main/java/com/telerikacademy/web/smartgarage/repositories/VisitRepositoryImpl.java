package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.InvalidRequestParamsException;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class VisitRepositoryImpl extends AbstractGenericCRUDRepository<Visit> implements VisitRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Visit> getAll() {
        return super.getAll(Visit.class);
    }

    @Override
    public Visit getById(int id) {
        return super.getByField("id", id, Visit.class);
    }

    @Override
    public void create(Visit visitToCreate) {
        super.create(visitToCreate);
    }

    @Override
    public void update(Visit visitToUpdate) {
        super.update(visitToUpdate);
    }

    @Override
    public List<Visit> filter(double rate, VisitFilterParameters vfp) {
        try (Session session = sessionFactory.openSession()) {
            String baseQuery =
                    "select vi from Visit vi " +
                            "join Vehicle ve on vi.vehicle=ve " +
                            "join User u on ve.user=u ";

            var filters = new ArrayList<String>();
            if (vfp.getUserId() != null) {
                filters.add(" u.id=:userId ");
            }
            vfp.getVehicleId().ifPresent(vi -> filters.add(" ve.id = :vehicleId "));
            vfp.getRegistrationPlate().ifPresent(vr -> filters.add(" ve.registrationPlate = :registrationPlate "));
            vfp.getBeginDate().ifPresent(b -> filters.add(" vi.beginDate >= :beginDate "));
            vfp.getEndDate().ifPresent(e -> filters.add(" vi.endDate <= :endDate "));

            if (!filters.isEmpty()) {
                baseQuery = baseQuery + " where " + String.join(" and ", filters);
            }

            Query<Visit> query = session.createQuery(baseQuery, Visit.class);
            if (vfp.getUserId() != null) {
                query.setParameter("userId", vfp.getUserId());
            }
            vfp.getRegistrationPlate().ifPresent(registrationPlate -> query.setParameter("registrationPlate", registrationPlate));
            vfp.getVehicleId().ifPresent(vehicleId -> query.setParameter("vehicleId", vehicleId));
            vfp.getBeginDate().ifPresent(beginDate -> query.setParameter("beginDate", LocalDate.parse(beginDate)));
            vfp.getEndDate().ifPresent(endDate -> query.setParameter("endDate", LocalDate.parse(endDate)));

            List<Visit> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Visit", "similar", "parameters");
            }
            for (Visit visit : result) {
                visit.setTotalPrice(Math.round(visit.getTotalPrice() * rate * 100.00) / 100.00);
            }

            return query.getResultList();
        }
    }

    @Override
    public List<Visit> sort(Optional<String> beginDate, Optional<String> endDate) {
        boolean beginDateIsPresent = beginDate.isPresent();
        boolean endDateIsPresent = endDate.isPresent();
        String baseQuery = "from Visit order by ";
        if (beginDateIsPresent) {
            baseQuery += " beginDate " + beginDate.get();
        }
        if (endDateIsPresent) {
            baseQuery += " endDate " + endDate.get();
        }
        if (beginDateIsPresent && endDateIsPresent) {
            throw new InvalidRequestParamsException("Choose between beginDate or endDate carefully!");
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(baseQuery, Visit.class);
            List<Visit> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Visit");
            }
            return result;
        }
    }

    @Override
    public void disableVisits(int vehicleId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            String disableQuery = " update Visit set enabled = false where vehicle.id = :vehicleId ";

            var query = session.createQuery(disableQuery).
                    setParameter("vehicleId", vehicleId).
                    executeUpdate();
            session.getTransaction().commit();
        }
    }

}
