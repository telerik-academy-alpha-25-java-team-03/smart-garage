package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.dtos.UserDto;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import com.telerikacademy.web.smartgarage.services.contracts.UserService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final ModelService modelService;
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(ModelService modelService,
                             UserService userService,
                             ModelMapper modelMapper,
                             AuthenticationHelper authenticationHelper) {
        this.modelService = modelService;
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @ModelAttribute("allModels")
    public List<com.telerikacademy.web.smartgarage.models.Model> allModels(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        return modelService.getAll(currentUser);
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (user.hasRights()) {
                List<User> users = userService.getAll(user);
                model.addAttribute("users", users);
                model.addAttribute("currentUser", user);
                return "users";
            }
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
            return "unauthorized";
        }
        return "redirect:/";
    }

    @GetMapping("/create")
    public String create(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("userDto", new UserDto());
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
        return "user-create";
    }

    @PostMapping("/create")
    public String handleCreate(@Valid @ModelAttribute("userDto") UserDto dto,
                               HttpSession session, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user-create";
        }
        try {
            User admin = authenticationHelper.tryGetUser(session);
            User newUser = modelMapper.fromDto(dto);
            userService.create(admin, newUser);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        } catch (DuplicateEntityException d) {
            bindingResult.rejectValue("email", "duplicate_email", d.getMessage());
            return "user-create";
        }
        return "redirect:/users";
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = userService.getById(authenticationHelper.tryGetUser(session), id);
            model.addAttribute("currentUser", user);

            UserDto userDto = modelMapper.toUserDto(user);
            model.addAttribute("userDto", userDto);

            return "user-update";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        } catch (EntityNotFoundException d) {
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("userDto") UserDto userDto,
                             HttpSession session,
                             BindingResult errors) {
        if (errors.hasErrors()) {
            return "user-update";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            User user = modelMapper.fromDto(userDto, id);
            userService.update(currentUser, user);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("userDto.email", "email.exists", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
    }

    @GetMapping("/{id}/delete")
    public String handleDeleteUser(Model model,
                                   HttpSession session,
                                   @PathVariable int id) {
        try {
            User admin = authenticationHelper.tryGetUser(session);
            User user = userService.getById(admin, id);
            userService.delete(admin, id);
            return "redirect:/users";
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }

}
