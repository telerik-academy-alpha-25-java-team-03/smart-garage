package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.enums.UserRoles;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.web.smartgarage.utilities.AuthorizationHelper.verifyUserHasRoles;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Model> getAll(User user) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return modelRepository.getAll();
    }

    @Override
    public Model getById(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return modelRepository.getById(id);
    }

    @Override
    public Model getByField(User user, String name) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return modelRepository.getByField(name);
    }

    @Override
    public void create(User user, Model model) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        try {
            modelRepository.getByField(model.getName());
        } catch (EntityNotFoundException e) {
            modelRepository.create(model);
        }
    }

}
