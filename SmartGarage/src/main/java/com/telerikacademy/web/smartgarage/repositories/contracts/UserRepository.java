package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CRURepositoryOperations<User> {

    User getByEmail(String email);

    List<User> filter(UserFilterParameters ufp);

    List<User> sort(Optional<String> firstName, Optional<String> beginDate, Optional<String> endDate);

}
