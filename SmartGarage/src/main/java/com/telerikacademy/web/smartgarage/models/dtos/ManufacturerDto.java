package com.telerikacademy.web.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ManufacturerDto {

    @NotNull(message = "Model name cannot be empty!")
    @Size(min = 2, max = 20, message = "Manufacturer name must be between 2 and 20 symbols!")
    private String name;

    public ManufacturerDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
