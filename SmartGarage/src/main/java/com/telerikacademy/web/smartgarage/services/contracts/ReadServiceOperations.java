package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.User;

import java.util.List;

public interface ReadServiceOperations<T> {

    List<T> getAll(User object);

    T getById(User user, int id);

    T getByField(User user, String field);

}
