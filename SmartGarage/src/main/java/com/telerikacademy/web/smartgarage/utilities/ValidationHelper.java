package com.telerikacademy.web.smartgarage.utilities;

import com.telerikacademy.web.smartgarage.exceptions.InvalidRequestParamsException;

import java.util.Locale;
import java.util.Optional;

public class ValidationHelper {

    private ValidationHelper() {
    }

    public static void verifyIsLegal(Optional<String> object) {
        if (object.isPresent() &&
                !(object.get().equalsIgnoreCase("asc") || object.get().equalsIgnoreCase("desc"))) {
            throw new InvalidRequestParamsException("Invalid request parameter!");
        }
    }

    public static double getRate(String currency) {
        if (!currency.isEmpty()) {
            CurrencyExchanger currencyExchanger = new CurrencyExchanger();
            return currencyExchanger.getRate(currency.toUpperCase(Locale.ROOT));
        } else return 1;
    }

}
