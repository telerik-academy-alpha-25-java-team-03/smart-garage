package com.telerikacademy.web.smartgarage.utilities;

import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.dtos.*;
import com.telerikacademy.web.smartgarage.models.filterParameters.OfferedServiceFilterParameters;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;
import com.telerikacademy.web.smartgarage.models.filterParameters.VehicleFilterParameters;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class ModelMapper {

    private final UserRepository userRepository;
    private final ManufacturerRepository manufacturerRepository;
    private final ModelRepository modelRepository;
    private final VehicleRepository vehicleRepository;
    private final VisitRepository visitRepository;
    private final OfferedServiceRepository offeredServiceRepository;

    @Autowired
    public ModelMapper(UserRepository userRepository,
                       ManufacturerRepository manufacturerRepository,
                       ModelRepository modelRepository,
                       VehicleRepository vehicleRepository,
                       VisitRepository visitRepository,
                       OfferedServiceRepository offeredServiceRepository) {
        this.userRepository = userRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.modelRepository = modelRepository;
        this.vehicleRepository = vehicleRepository;
        this.visitRepository = visitRepository;
        this.offeredServiceRepository = offeredServiceRepository;
    }

    public UserFilterParameters fromParametersList(Optional<String> firstName,
                                                   Optional<String> lastName,
                                                   Optional<String> email,
                                                   Optional<String> phoneNumber,
                                                   Optional<Integer> vehicleModelId,
                                                   Optional<String> beginDate,
                                                   Optional<String> endDate) {
        return new UserFilterParameters()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setEmail(email)
                .setPhoneNumber(phoneNumber)
                .setVehicleModelId(vehicleModelId)
                .setBeginDate(beginDate)
                .setEndDate(endDate);
    }

    public OfferedServiceFilterParameters fromParametersList(Optional<String> name,
                                                             Optional<Double> minPrice,
                                                             Optional<Double> maxPrice) {
        return new OfferedServiceFilterParameters()
                .setName(name)
                .setMinPrice(minPrice)
                .setMaxPrice(maxPrice);
    }

    public VisitFilterParameters fromVisitParametersList(Integer userId,
                                                         Optional<Integer> vehicleId,
                                                         Optional<String> registrationPlate,
                                                         Optional<String> startDate,
                                                         Optional<String> endDate) {
        return new VisitFilterParameters()
                .setUserId(userId)
                .setVehicleId(vehicleId)
                .setRegistrationPlate(registrationPlate)
                .setBeginDate(startDate)
                .setEndDate(endDate);
    }

    public VehicleFilterParameters fromVehicleParametersList(Optional<String> firstName,
                                                             Optional<String> lastName,
                                                             Optional<String> email) {
        return new VehicleFilterParameters()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setEmail(email);
    }

    // FROM DTO WITHOUT ID
    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public Manufacturer fromDto(ManufacturerDto manufacturerDto) {
        Manufacturer manufacturer = new Manufacturer();
        dtoToObject(manufacturerDto, manufacturer);
        return manufacturer;
    }

    public Model fromDto(ModelDto modelDto) {
        Model model = new Model();
        dtoToObject(modelDto, model);
        return model;
    }

    public Vehicle fromDto(VehicleDto vehicleDto) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(vehicleDto, vehicle);
        return vehicle;
    }

    public Visit fromDto(VisitDto visitDto) {
        Visit visit = new Visit();
        dtoToObject(visitDto, visit);
        return visit;
    }

    public OfferedService fromDto(OfferedServiceDto dto) {
        var offeredService = new OfferedService();
        dtoToObject(dto, offeredService);
        return offeredService;
    }

    public VisitFilterParameters fromDto(VisitFilterParamsDto dto) {
        VisitFilterParameters vfp = new VisitFilterParameters();
        vfp.setRegistrationPlate(dto.getRegistrationPlate());
        vfp.setBeginDate(dto.getBeginDate());
        vfp.setVehicleId(dto.getVehicleId());
        vfp.setUserId(dto.getUserId());
        vfp.setEndDate(dto.getEndDate());
        return vfp;
    }

    // FROM DTO WITH ID
    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    public Manufacturer fromDto(ManufacturerDto manufacturerDto, int id) {
        Manufacturer manufacturer = manufacturerRepository.getById(id);
        dtoToObject(manufacturerDto, manufacturer);
        return manufacturer;
    }

    public Model fromDto(ModelDto modelDto, int id) {
        Model model = modelRepository.getById(id);
        dtoToObject(modelDto, model);
        return model;
    }

    public Vehicle fromDto(VehicleDto vehicleDto, int id) {
        Vehicle vehicle = vehicleRepository.getById(id);
        dtoToObject(vehicleDto, vehicle);
        return vehicle;
    }

    public Visit fromDto(VisitDto visitDto, int id) {
        Visit visit = visitRepository.getById(id);
        dtoToObject(visitDto, visit);
        return visit;
    }

    public OfferedService fromDto(OfferedServiceDto dto, int id) {
        var offeredService = offeredServiceRepository.getById(id);
        dtoToObject(dto, offeredService);
        return offeredService;
    }

    //TO DTO
    public UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setEmail(user.getEmail());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setPhoneNumber(user.getPhoneNumber());
        //role
        return userDto;
    }

    public OfferedServiceDto toOfferedServiceDto(OfferedService offeredService) {
        OfferedServiceDto offeredServiceDto = new OfferedServiceDto();
        offeredServiceDto.setName(offeredService.getName());
        offeredServiceDto.setPrice(offeredService.getPrice());
        return offeredServiceDto;
    }

    public VehicleDto toVehicleDto(Vehicle vehicle) {
        VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setRegistrationPlate(vehicle.getRegistrationPlate());
        vehicleDto.setVin(vehicle.getVin());
        vehicleDto.setYear(vehicle.getYear());
        vehicleDto.setUserId(vehicle.getUser().getId());
        vehicleDto.setModelDto(toModelDto(vehicle.getModel()));
        return vehicleDto;
    }

    public VisitDto toVisitDto(Visit visit) {
        VisitDto visitDto = new VisitDto();
        visitDto.setBeginDate(visit.getBeginDate().toString());
        visitDto.setEndDate(visit.getEndDate().toString());
        visitDto.setVehicleId(visit.getVehicle().getId());
        visitDto.setOfferedServicesIds(visit.getOfferedServices().toString());
        return visitDto;
    }

    public ModelDto toModelDto(Model model) {
        ModelDto modelDto = new ModelDto();
        modelDto.setManufacturerDto(toManufacturerDto(model.getManufacturer()));
        modelDto.setName(model.getName());
        return modelDto;
    }

    public ManufacturerDto toManufacturerDto(Manufacturer manufacturer) {
        ManufacturerDto manufacturerDto = new ManufacturerDto();
        manufacturerDto.setName(manufacturer.getName());
        return manufacturerDto;
    }

    // DTO TO OBJECT
    private void dtoToObject(UserDto userDto, User user) {
        Role role = new Role();
        role.setId(1);
        user.getRoles().add(role);
        user.setEnabled(true);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
    }

    private void dtoToObject(ManufacturerDto manufacturerDto, Manufacturer manufacturer) {
        manufacturer.setEnabled(true);
        manufacturer.setName(manufacturerDto.getName());
    }

    private void dtoToObject(ModelDto modelDto, Model model) {
        Manufacturer manufacturer = manufacturerRepository.getByField(modelDto.getManufacturerDto().getName());
        model.setEnabled(true);
        model.setName(modelDto.getName());
        model.setManufacturer(manufacturer);
    }

    private void dtoToObject(VehicleDto vehicleDto, Vehicle vehicle) {
        Model model = modelRepository.getByField(vehicleDto.getModelDto().getName());
        User user = userRepository.getById(vehicleDto.getUserId());
        vehicle.setEnabled(true);
        vehicle.setRegistrationPlate(vehicleDto.getRegistrationPlate());
        vehicle.setVin(vehicleDto.getVin());
        vehicle.setYear(vehicleDto.getYear());
        vehicle.setModel(model);
        vehicle.setUser(user);
    }

    private void dtoToObject(VisitDto visitDto, Visit visit) {
        visit.setEnabled(true);
        visit.setBeginDate(LocalDate.parse(visitDto.getBeginDate()));
        visit.setEndDate(LocalDate.parse(visitDto.getEndDate()));
        visit.setStatus();
        Set<OfferedService> offeredServices = new HashSet<>();
        String[] offeredServicesIds = visitDto.getOfferedServicesIds().split(",");
        for (String id : offeredServicesIds) {
            offeredServices.add(offeredServiceRepository.getById(Integer.parseInt(id)));
        }
        visit.setOfferedServices(offeredServices);
        double totalPrice = offeredServices.stream()
                .mapToDouble(OfferedService::getPrice)
                .sum();
        visit.setTotalPrice(totalPrice);
        Vehicle vehicle = vehicleRepository.getById(visitDto.getVehicleId());
        visit.setVehicle(vehicle);
    }

    private void dtoToObject(OfferedServiceDto dto, OfferedService offeredService) {
        offeredService.setEnabled(true);
        offeredService.setName(dto.getName());
        offeredService.setPrice(dto.getPrice());
    }

}
