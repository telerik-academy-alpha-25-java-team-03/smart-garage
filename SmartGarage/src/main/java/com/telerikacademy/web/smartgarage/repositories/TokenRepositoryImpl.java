package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.ExpiryTokenFailureException;
import com.telerikacademy.web.smartgarage.models.PasswordResetToken;
import com.telerikacademy.web.smartgarage.repositories.contracts.TokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class TokenRepositoryImpl implements TokenRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PasswordResetToken createToken() {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            PasswordResetToken token = new PasswordResetToken();
            session.save(token);
            session.getTransaction().commit();
            return token;
        }
    }

    @Override
    public boolean isExpired(String tokenText) {
        PasswordResetToken token = getToken(tokenText);
        if (token != null) {
            token.getExpiryTime().after(new Date(System.currentTimeMillis()));
            return true;
        } else {
            throw new ExpiryTokenFailureException("Token is out of time!");
        }
    }

    @Override
    public PasswordResetToken getToken(String tokenText) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<PasswordResetToken> query = session.createQuery(
                    "from PasswordResetToken where " +
                            " token = :tokenText and accessed = false ", PasswordResetToken.class);
            query.setParameter("tokenText", tokenText);
            List<PasswordResetToken> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("There is no such token!");
            }
            return result.get(0);
        }
    }

    @Override
    public void markTokenAsAccessed(String tokenText) {
        try (Session session = sessionFactory.openSession()) {
            Integer targetId = getToken(tokenText).getId();
            session.beginTransaction();
            String disableQuery = " update PasswordResetToken set accessed = true where id = :targetId";
            var query = session.createQuery(disableQuery, PasswordResetToken.class).
                    setParameter("targetId", targetId).
                    executeUpdate();
            session.getTransaction().commit();
        }
    }
}
