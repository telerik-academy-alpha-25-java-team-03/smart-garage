package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.enums.UserRoles;
import com.telerikacademy.web.smartgarage.models.filterParameters.VehicleFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.web.smartgarage.utilities.AuthorizationHelper.verifyUserHasRoles;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Vehicle> getAll(User user) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return vehicleRepository.getAll();
    }

    @Override
    public Vehicle getById(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return vehicleRepository.getById(id);
    }

    @Override
    public Vehicle getByField(User user, String registrationPlate) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return vehicleRepository.getByField(registrationPlate);
    }

    @Override
    public Vehicle getByVin(User user, String vin) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return vehicleRepository.getByVin(vin);
    }

    @Override
    public void create(User user, Vehicle vehicle) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        validateExisting(vehicle);
        vehicleRepository.create(vehicle);
    }

    @Override
    public void update(User user, Vehicle vehicle) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        validateUnique(vehicle);
        vehicleRepository.update(vehicle);
    }

    @Override
    public void delete(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        Vehicle existing = vehicleRepository.getById(id);
        existing.setEnabled(false);
        vehicleRepository.update(existing);
    }

    @Override
    public List<Vehicle> filterByOwner(User user, VehicleFilterParameters vfp) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return vehicleRepository.filterByOwner(vfp);
    }

    private void validateExisting(Vehicle vehicle) {
        boolean duplicateExists = true;
        try {
            vehicleRepository.getByField(vehicle.getRegistrationPlate());
        } catch (EntityNotFoundException er) {
            try {
                vehicleRepository.getByVin(vehicle.getVin());
            } catch (EntityNotFoundException ev) {
                duplicateExists = false;
            }
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle", "same", "parameters");
        }
    }

    private void validateUnique(Vehicle vehicle) {
        boolean duplicateExists = true;
        try {
            Vehicle existing = vehicleRepository.getByField(vehicle.getRegistrationPlate());
            if (existing.getId() == vehicle.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle");
        }
    }

}
