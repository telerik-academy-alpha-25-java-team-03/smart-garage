package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ManufacturerRepositoryImpl
        extends AbstractGenericGetRepository<Manufacturer> implements ManufacturerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ManufacturerRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory1;
    }

    @Override
    public List<Manufacturer> getAll() {
        return super.getAll(Manufacturer.class);
    }

    @Override
    public Manufacturer getById(int id) {
        return super.getByField("id", id, Manufacturer.class);
    }

    @Override
    public Manufacturer getByField(String name) {
        return super.getByField("name", name, Manufacturer.class);
    }

    @Override
    public void create(Manufacturer manufacturer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(manufacturer);
            session.getTransaction().commit();
        }
    }

}
