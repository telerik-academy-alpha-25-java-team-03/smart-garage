package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.User;

public interface CRUDServiceOperations<T> extends ReadServiceOperations<T> {

    void create(User user, T object);

    void update(User user, T object);

    void delete(User user, int id);

}
