package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;

import java.util.List;
import java.util.Optional;

public interface UserService extends CRUDServiceOperations<User> {

    User getByEmail(String email);

    List<User> filter(User user, UserFilterParameters ufp);

    List<User> sort(User user, Optional<String> firstName, Optional<String> beginDate, Optional<String> endDate);

    String resetPassword(String email);

    String resetPasswordConfirm(String token, String email, String password, String confirmPassword);

}
