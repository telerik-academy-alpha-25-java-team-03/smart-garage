package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.dtos.OfferedServiceDto;
import com.telerikacademy.web.smartgarage.services.contracts.OfferedServiceService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(tags = "Offered Services")
@RestController
@RequestMapping("/api/services")
public class OfferedServiceController {

    private final OfferedServiceService service;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public OfferedServiceController(OfferedServiceService service,
                                    ModelMapper modelMapper,
                                    AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Find all Offered Services",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<OfferedService> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return service.getAll(user);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find Offered Services by Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public OfferedService getById(@RequestHeader HttpHeaders headers,
                                  @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return service.getById(user, id);
    }

    @GetMapping("/name")
    @ApiOperation(value = "Find Offered Service by name",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public OfferedService getByName(@RequestHeader HttpHeaders headers,
                                    @RequestParam String name) {
        User user = authenticationHelper.tryGetUser(headers);
        return service.getByField(user, name);
    }

    @GetMapping("/filter")
    @ApiOperation(value = "Filter Offered Services by their name, min price and max price",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<OfferedService> filter(@RequestHeader HttpHeaders headers,
                                       @RequestParam(required = false) Optional<String> name,
                                       @RequestParam(required = false) Optional<Double> minPrice,
                                       @RequestParam(required = false) Optional<Double> maxPrice) {
        User user = authenticationHelper.tryGetUser(headers);
        var filterParameters = modelMapper.fromParametersList(name, minPrice, maxPrice);
        return service.filter(user, filterParameters);
    }

    @PostMapping
    @ApiOperation(value = "Create new Offered Service",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public OfferedService create(@RequestHeader HttpHeaders headers,
                                 @Valid @RequestBody OfferedServiceDto offeredServiceDto) {
        User user = authenticationHelper.tryGetUser(headers);
        var offeredService = modelMapper.fromDto(offeredServiceDto);
        service.create(user, offeredService);
        return offeredService;
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update given Offered Service by his Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public OfferedService update(@RequestHeader HttpHeaders headers,
                                 @PathVariable int id,
                                 @Valid @RequestBody OfferedServiceDto offeredServiceDto) {
        User user = authenticationHelper.tryGetUser(headers);
        var offeredService = modelMapper.fromDto(offeredServiceDto, id);
        service.update(user, offeredService);
        return offeredService;
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete given Offered Service by his Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        service.delete(user, id);
    }

    @GetMapping("/pdf-report")
    @ApiOperation(value = "Generate report for visit and currency",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public String generateReportPDF(@RequestHeader HttpHeaders headers,
                                    @RequestParam(required = false) Optional<String> currency,
                                    @RequestParam(required = false) Optional<Integer> vehicleId,
                                    @RequestParam(required = false) Optional<String> registrationPlate,
                                    @RequestParam(required = false) Optional<String> beginDate,
                                    @RequestParam(required = false) Optional<String> endDate) {
        User user = authenticationHelper.tryGetUser(headers);
        var vfp = modelMapper.fromVisitParametersList(user.getId(), vehicleId, registrationPlate, beginDate, endDate);
        return service.generateReportPDF(user, currency, vfp);
    }

}
