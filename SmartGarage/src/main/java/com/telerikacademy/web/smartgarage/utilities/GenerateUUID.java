package com.telerikacademy.web.smartgarage.utilities;

import java.util.UUID;

public class GenerateUUID {

    public String getUUID() {
        UUID myUuid = UUID.randomUUID();
        return myUuid.toString();
    }
}
