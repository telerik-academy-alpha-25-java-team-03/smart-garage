package com.telerikacademy.web.smartgarage.models.filterParameters;

import java.util.Optional;

public class OfferedServiceFilterParameters {

    private Optional<String> name;
    private Optional<Double> minPrice;
    private Optional<Double> maxPrice;

    public Optional<String> getName() {
        return name;
    }

    public OfferedServiceFilterParameters setName(Optional<String> name) {
        this.name = name;
        return this;
    }

    public Optional<Double> getMinPrice() {
        return minPrice;
    }

    public OfferedServiceFilterParameters setMinPrice(Optional<Double> minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    public Optional<Double> getMaxPrice() {
        return maxPrice;
    }

    public OfferedServiceFilterParameters setMaxPrice(Optional<Double> maxPrice) {
        this.maxPrice = maxPrice;
        return this;
    }
}
