package com.telerikacademy.web.smartgarage.models.enums;

public enum UserRoles {
    ADMIN,
    EMPLOYEE,
    CUSTOMER;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
