package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.PasswordResetToken;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.enums.UserRoles;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.web.smartgarage.services.contracts.TokenService;
import com.telerikacademy.web.smartgarage.services.contracts.UserService;
import com.telerikacademy.web.smartgarage.utilities.GenerateUUID;
import com.telerikacademy.web.smartgarage.utilities.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.smartgarage.utilities.AuthorizationHelper.verifyUserHasRoles;
import static com.telerikacademy.web.smartgarage.utilities.ValidationHelper.verifyIsLegal;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final VehicleRepository vehicleRepository;
    private final MailSender mailSender;
    private final TokenService tokenService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           VehicleRepository vehicleRepository,
                           JavaMailSender sender,
                           TokenService tokenService) {
        this.userRepository = userRepository;
        this.vehicleRepository = vehicleRepository;
        this.mailSender = new MailSender(sender);
        this.tokenService = tokenService;
    }

    @Override
    public List<User> getAll(User user) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return userRepository.getAll();
    }

    @Override
    public User getById(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return userRepository.getById(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User getByField(User user, String firstName) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return userRepository.getByField(firstName);
    }

    @Override
    public void create(User userToVerify, User user) {
        verifyUserHasRoles(userToVerify, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        validateExisting(user);
        userRepository.create(user);
        mailSender.sendEmailToNewCreatedUser(user.getEmail(),
                user.getFirstName(), user.getPassword());
        System.out.println("Done!");
    }

    @Override
    public void update(User userToVerify, User user) {
        verifyUserHasRoles(userToVerify, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        validateUnique(user);
        userRepository.update(user);
    }

    @Override
    public void delete(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        User existing = userRepository.getById(id);
        existing.setEnabled(false);
        if (existing.getVehicles().size() != 0) {
            vehicleRepository.disableVehicles(id);
        }
        userRepository.update(existing);
    }

    @Override
    public String resetPassword(String email) {
        User user = userRepository.getByEmail(email);
        PasswordResetToken token = tokenService.createToken();
        mailSender.sendEmailForResetPassword(
                user.getEmail(),
                user.getFirstName(),
                "http://localhost:8080/reset-password", token.getToken());
        return "You will receive email with instructions!";
    }

    @Override
    public String resetPasswordConfirm(String token, String email, String password, String confirmPassword) {
        User user = userRepository.getByEmail(email);
        if (password.equals(confirmPassword)) {
            user.setPassword(password);
            userRepository.update(user);
            mailSender.sendEmailSuccessfulPasswordChange(user.getEmail(), user.getFirstName());
            //tokenService.markTokenAsAccessed(token);
            return "Successful password update!";
        } else {
            throw new AuthenticationFailureException("The passwords do not match!");
        }
    }

    @Override
    public List<User> filter(User user, UserFilterParameters ufp) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return userRepository.filter(ufp);
    }

    @Override
    public List<User> sort(User user,
                           Optional<String> firstName,
                           Optional<String> beginDate,
                           Optional<String> endDate) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        verifyIsLegal(firstName);
        verifyIsLegal(beginDate);
        verifyIsLegal(endDate);
        return userRepository.sort(firstName, beginDate, endDate);
    }

    private void validateExisting(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        GenerateUUID uuid = new GenerateUUID();
        String generatePassword = uuid.getUUID().substring(0, 12);
        user.setPassword(generatePassword);
    }

    private void validateUnique(User user) {
        boolean duplicateExists = true;
        try {
            User existing = userRepository.getByEmail(user.getEmail());
            if (existing.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User");
        }
    }

}
