package com.telerikacademy.web.smartgarage.repositories.contracts;

import java.util.List;

public interface ReadRepositoryOperations<T> {

    List<T> getAll();

    T getById(int id);

    T getByField(String field);

}
