package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.dtos.VisitDto;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(tags = "Visits")
@RestController
@RequestMapping("/api/visits")
public class VisitController {

    private final VisitService visitService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VisitController(VisitService visitService,
                           ModelMapper modelMapper,
                           AuthenticationHelper authenticationHelper) {
        this.visitService = visitService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Find all Visits",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<Visit> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return visitService.getAll(user);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find Visit by Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public Visit getById(@RequestHeader HttpHeaders headers,
                         @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return visitService.getById(user, id);
    }

    @GetMapping("/sort")
    @ApiOperation(value = "Sort Visit by begin date or end date order by ascending or descending",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<Visit> sort(@RequestHeader HttpHeaders headers,
                            @RequestParam Optional<String> beginDate,
                            @RequestParam Optional<String> endDate) {
        User user = authenticationHelper.tryGetUser(headers);
        return visitService.sort(user, beginDate, endDate);
    }

    @GetMapping("/filter")
    @ApiOperation(value = "Filter Visit by vehicle Id, registration plate, start date, end date," +
            " and option to choose a currency",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<Visit> filter(@RequestHeader HttpHeaders headers,
                              @RequestParam(required = false) Optional<String> currency,
                              @RequestParam(required = false) Optional<Integer> vehicleId,
                              @RequestParam(required = false) Optional<String> registrationPlate,
                              @RequestParam(required = false) Optional<String> startDate,
                              @RequestParam(required = false) Optional<String> endDate) {
        User user = authenticationHelper.tryGetUser(headers);
        Integer userId = user.getId();
        VisitFilterParameters vfp = modelMapper.fromVisitParametersList(userId, vehicleId, registrationPlate, startDate, endDate);
        return visitService.filter(user, currency, vfp);
    }

    @PostMapping
    @ApiOperation(value = "Create a new Visit",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public Visit create(@RequestHeader HttpHeaders headers,
                        @Valid @RequestBody VisitDto visitDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Visit visit = modelMapper.fromDto(visitDto);
        visitService.create(user, visit);
        return visit;
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Visit by given Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public Visit update(@RequestHeader HttpHeaders headers,
                        @PathVariable int id,
                        @Valid @RequestBody VisitDto visitDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Visit visit = modelMapper.fromDto(visitDto, id);
        visitService.update(user, visit);
        return visit;
    }

    @ApiOperation(value = "Delete Visit by given Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        visitService.delete(user, id);
    }

}
