package com.telerikacademy.web.smartgarage.models.dtos;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ModelDto {

    @NotNull(message = "Model name cannot be empty!")
    @Size(min = 2, max = 20, message = "Model name must be between 2 and 20 symbols!")
    private String name;

    @Valid
    private ManufacturerDto manufacturerDto;

    public ModelDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ManufacturerDto getManufacturerDto() {
        return manufacturerDto;
    }

    public void setManufacturerDto(ManufacturerDto manufacturerDto) {
        this.manufacturerDto = manufacturerDto;
    }
}
