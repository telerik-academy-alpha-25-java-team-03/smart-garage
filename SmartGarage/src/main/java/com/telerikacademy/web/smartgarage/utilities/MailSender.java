package com.telerikacademy.web.smartgarage.utilities;

import com.telerikacademy.web.smartgarage.models.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.List;
import java.util.Properties;

@Component
public class MailSender {

    private static final String WELCOME_TOPIC_CUSTOMER = "Welcome to SMG 03!";
    private static final String RESET_PASSWORD_CUSTOMER = "Reset your password";
    private static final String SUCCESSFUL_PASSWORD_RESET = "Smart Garage Password Change";
    private static final String PDF_REPORT = "Pdf report for your visits";
    private static final String SEND_EMAIL = "Sending email...";
    private static final String EMAIL_FROM = "SMG03.Support.Team";
    private static final String FINAL_REGARDS_MESSAGE = "Best regards,";
    private static final String FINAL_SUPPORT_MESSAGE = "SMG 03 Support Team";
    private static final String SUCCESSFUL_EMAIL_SEND = "Done !";

    private final JavaMailSender mailSender;

    @Autowired
    public MailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendEmailToNewCreatedUser(String toEmail, String userName, String password) {
        StringBuilder builder = new StringBuilder();
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(EMAIL_FROM);
        message.setTo(toEmail);
        message.setSubject(WELCOME_TOPIC_CUSTOMER);

        builder.append(String.format("Hello %s,%n", userName))
                .append(System.lineSeparator())
                .append(String.format("Your login password is: %s", password))
                .append(System.lineSeparator())
                .append("We recommend that when you log into your system for the first time to change it.")
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(FINAL_REGARDS_MESSAGE)
                .append(System.lineSeparator())
                .append(FINAL_SUPPORT_MESSAGE);
        message.setText(builder.toString());

        System.out.println(SEND_EMAIL);
        mailSender.send(message);
        System.out.println(SUCCESSFUL_EMAIL_SEND);
    }

    public void sendEmailForResetPassword(String toEmail, String userName, String link, String token) {
        StringBuilder builder = new StringBuilder();
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(EMAIL_FROM);
        message.setTo(toEmail);
        message.setSubject(SUCCESSFUL_PASSWORD_RESET);

        builder.append(String.format("Hello %s,%n To reset your password for Smart garage," +
                " please click the following link: %n", userName))
                .append(System.lineSeparator())
                .append(link)
                .append(String.format("%n and here is the access token for reset your password: %s", token))
                .append(String.format("%n If you don't want to reset your password, you can ignore this message" +
                        " - someone probably typed in your email address by mistake"))
                .append(FINAL_REGARDS_MESSAGE)
                .append(System.lineSeparator())
                .append(FINAL_SUPPORT_MESSAGE);

        message.setText(builder.toString());

        System.out.println(SEND_EMAIL);
        mailSender.send(message);
        System.out.println(SUCCESSFUL_EMAIL_SEND);
    }

    public void sendEmailSuccessfulPasswordChange(String toEmail, String userName) {
        StringBuilder builder = new StringBuilder();
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(EMAIL_FROM);
        message.setTo(toEmail);
        message.setSubject(RESET_PASSWORD_CUSTOMER);

        builder.append(String.format("Hello %s,%n This email is to notify you that " +
                "the password for the Smart Garage account %s has been changed. %n", userName, toEmail))
                .append(FINAL_REGARDS_MESSAGE)
                .append(System.lineSeparator())
                .append(FINAL_SUPPORT_MESSAGE);
        message.setText(builder.toString());

        System.out.println(SEND_EMAIL);
        mailSender.send(message);
        System.out.println(SUCCESSFUL_EMAIL_SEND);
    }

    public void sendReportViaEmail(String toEmail, List<Visit> visits, String abbreviatedCurrency, String fileName) {
//        String filePath = "C:\\Users\\Lenovo\\Desktop\\MyRepos\\smart-garage\\SmartGarage\\" + fileName;
        String filePath = "C:\\FILES\\TELERIK\\JAVA\\REPOS\\smart-garage\\SmartGarage\\" + fileName;
        StringBuilder builder = new StringBuilder();

        File newReport = new File(filePath);
        if (!newReport.exists() && !newReport.isDirectory()) {
            GeneratePDF pdf = new GeneratePDF();
            pdf.export(visits, abbreviatedCurrency);

            final String username = "smart.garage.a25@gmail.com";
            final String password = "dkuqvhocladbbpdq";

            Properties props = new Properties();
            props.put("mail.smtp.auth", true);
            props.put("mail.smtp.starttls.enable", true);
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            try {
                Message message = new MimeMessage(session);
                MimeBodyPart messageBodyPart = new MimeBodyPart();

                Multipart multipart = new MimeMultipart();
                message.setFrom(new InternetAddress(EMAIL_FROM));

                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(toEmail));

                message.setSubject(PDF_REPORT);
                builder.append("Hello, this is the report which you requested from our service.")
                        .append(System.lineSeparator())
                        .append("We remain available if you have more questions for us!")
                        .append("\n")
                        .append(FINAL_REGARDS_MESSAGE)
                        .append(System.lineSeparator())
                        .append(FINAL_SUPPORT_MESSAGE);
                message.setText(builder.toString());

                DataSource source = new FileDataSource(filePath);

                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(fileName);

                multipart.addBodyPart(messageBodyPart);
                message.setContent(multipart);

                System.out.println(SEND_EMAIL);
                Transport.send(message);
                System.out.println(SUCCESSFUL_EMAIL_SEND);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

}
