package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.filterParameters.OfferedServiceFilterParameters;

import java.util.List;

public interface OfferedServiceRepository extends CRURepositoryOperations<OfferedService> {

    List<OfferedService> filter(OfferedServiceFilterParameters ofp);

}
