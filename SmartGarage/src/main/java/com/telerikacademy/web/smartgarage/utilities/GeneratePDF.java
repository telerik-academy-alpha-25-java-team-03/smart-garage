package com.telerikacademy.web.smartgarage.utilities;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.telerikacademy.web.smartgarage.models.*;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.smartgarage.utilities.ValidationHelper.getRate;

public class GeneratePDF {

    private final static DecimalFormat df = new DecimalFormat("#0.00");

    public GeneratePDF() {
    }

    public void export(List<Visit> visits, String abbreviatedCurrency) {
        try {
            Document document = new Document(PageSize.A4);
            FileOutputStream fileOutputStream = new FileOutputStream("pdfReport.pdf");
            PdfWriter.getInstance(document, fileOutputStream);
            document.open();
            for (Visit visit : visits) {
                createPdf(document, visit);
            }

            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(100f);
            table.setWidths(new float[]{1.7f, 3.5f, 3.5f, 3.5f});
            table.setSpacingBefore(10);

            writeTableHeader(table, abbreviatedCurrency);
            writeTableData(table, visits, abbreviatedCurrency);

            document.add(table);

            document.close();
            fileOutputStream.close();
        } catch (DocumentException | IOException documentException) {
            documentException.printStackTrace();
        }
    }

    private void createPdf(Document document, Visit visit) {

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setSize(11);
        font.setColor(Color.DARK_GRAY);

        User user = visit.getVehicle().getUser();
        Vehicle vehicle = visit.getVehicle();
        Model model = visit.getVehicle().getModel();

        Paragraph headerParagraph = new Paragraph("DETAIL REPORT FOR SERVICES", new Font());
        headerParagraph.setAlignment(Paragraph.ALIGN_CENTER);
        headerParagraph.setExtraParagraphSpace(2.0f);
        document.add(headerParagraph);
        document.add(emptyParagraph(2));

        Paragraph userParagraph = new Paragraph("USER INFORMATION", font);
        userParagraph.setAlignment(Paragraph.ALIGN_LEFT);
        userParagraph.setExtraParagraphSpace(2.0f);
        userParagraph.add(emptyParagraph(1));
        userParagraph.add(new Paragraph(String.format("User id: %s", user.getId())));
        userParagraph.add(new Paragraph(String.format("User Name: %s %s", user.getFirstName(), user.getLastName())));
        userParagraph.add(new Paragraph(String.format("User email: %s", user.getEmail())));
        userParagraph.add(new Paragraph(String.format("Phone Number: '%s'", user.getPhoneNumber())));
        document.add(new LineSeparator());
        document.add(userParagraph);
        document.add(emptyParagraph(2));

        Paragraph vehicleParagraph = new Paragraph("VEHICLE INFORMATION", font);
        vehicleParagraph.setAlignment(Paragraph.ALIGN_LEFT);
        vehicleParagraph.setExtraParagraphSpace(2.0f);
        vehicleParagraph.add(emptyParagraph(1));
        vehicleParagraph.add(new Paragraph(
                String.format("Vehicle id: %s", vehicle.getId())));
        vehicleParagraph.add(new Paragraph(
                String.format("Vehicle registration plate: %s", vehicle.getRegistrationPlate())));
        vehicleParagraph.add(new Paragraph(
                String.format("Vehicle VIN: %s", vehicle.getVin())));
        vehicleParagraph.add(new Paragraph(
                String.format("Vehicle model: %s [%s]", model.getName(), model.getManufacturer().getName())));
        document.add(vehicleParagraph);
        document.add(emptyParagraph(2));

        Paragraph visitParagraph = new Paragraph("VISIT INFORMATION", font);
        visitParagraph.setAlignment(Paragraph.ALIGN_LEFT);
        visitParagraph.setExtraParagraphSpace(2.0f);
        visitParagraph.add(emptyParagraph(1));
        visitParagraph.add(new Paragraph(
                String.format("Start date: %s", visit.getBeginDate().toString())));
        visitParagraph.add(new Paragraph(
                String.format("End date: %s", visit.getEndDate().toString())));
        visitParagraph.add(new Paragraph(
                String.format("Status: %s ", visit.getVisitStatus())));
        document.add(visitParagraph);
        document.add(emptyParagraph(2));

    }

    private void writeTableHeader(PdfPTable table, String abbreviatedCurrency) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLUE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(5);
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.BLACK);
        cell.setPhrase(new Phrase("Service Id", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Name", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Price (BGN)", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase(String.format("Price (%s)", abbreviatedCurrency), font));
        table.addCell(cell);

    }

    private void writeTableData(PdfPTable table, List<Visit> visits, String currency) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(5);

        List<OfferedService> services = new ArrayList<>(visits.get(0).getOfferedServices());
        for (OfferedService service : services) {
            cell.setPhrase(new Phrase(String.valueOf(service.getId()), new Font()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(service.getName(), new Font()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(String.valueOf(df.format(service.getPrice())), new Font()));
            table.addCell(cell);
            cell.setPhrase(new Phrase(String.valueOf(df.format(service.getPrice() * getRate(currency))), new Font()));
            table.addCell(cell);
        }
        cell.setPhrase(new Phrase("Total ", new Font()));
        table.addCell(cell);
        cell.setPhrase(new Phrase(" ", new Font()));
        table.addCell(cell);
        cell.setPhrase(new Phrase(String.valueOf(df.format(visits.get(0).getTotalPrice() * getRate(""))), new Font()));
        table.addCell(cell);
        cell.setPhrase(new Phrase(String.valueOf(df.format(visits.get(0).getTotalPrice())), new Font()));
        table.addCell(cell);
    }

    private Paragraph emptyParagraph(int line) {
        Paragraph paragraph = new Paragraph();
        while (line > 0) {
            paragraph.add(new Paragraph(" "));
            line--;
        }
        return paragraph;
    }

}
