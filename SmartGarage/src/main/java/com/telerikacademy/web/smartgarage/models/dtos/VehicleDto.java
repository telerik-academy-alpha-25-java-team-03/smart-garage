package com.telerikacademy.web.smartgarage.models.dtos;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class VehicleDto {

    @NotNull(message = "Registration plate cannot be empty!")
    @Size(min = 7, max = 8, message = "Registration plate must be between 7 and 8 symbols!")
    private String registrationPlate;

    @NotNull(message = "VIN number cannot be empty!")
    @Size(min = 17, max = 17, message = "VIN number must be 17 symbols!")
    private String vin;

    @Positive(message = "Vehicle year must be a positive number!")
    private int year;

    @Valid
    private ModelDto modelDto;

    @Positive
    private int userId;

    public VehicleDto() {
    }

    public String getRegistrationPlate() {
        return registrationPlate;
    }

    public void setRegistrationPlate(String registrationPlate) {
        this.registrationPlate = registrationPlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public ModelDto getModelDto() {
        return modelDto;
    }

    public void setModelDto(ModelDto modelDto) {
        this.modelDto = modelDto;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
