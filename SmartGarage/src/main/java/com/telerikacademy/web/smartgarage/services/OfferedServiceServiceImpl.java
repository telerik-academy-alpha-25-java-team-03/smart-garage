package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.InvalidRequestParamsException;
import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.enums.UserRoles;
import com.telerikacademy.web.smartgarage.models.filterParameters.OfferedServiceFilterParameters;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.OfferedServiceRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import com.telerikacademy.web.smartgarage.services.contracts.OfferedServiceService;
import com.telerikacademy.web.smartgarage.utilities.CurrencyExchanger;
import com.telerikacademy.web.smartgarage.utilities.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static com.telerikacademy.web.smartgarage.utilities.AuthorizationHelper.verifyUserHasRoles;

@Service
public class OfferedServiceServiceImpl implements OfferedServiceService {

    private final OfferedServiceRepository offeredServiceRepository;
    private final VisitRepository visitRepository;
    private final MailSender mailSender;

    @Autowired
    public OfferedServiceServiceImpl(OfferedServiceRepository offeredServiceRepository,
                                     VisitRepository visitRepository,
                                     JavaMailSender sender) {
        this.offeredServiceRepository = offeredServiceRepository;
        this.visitRepository = visitRepository;
        this.mailSender = new MailSender(sender);
    }

    @Override
    public List<OfferedService> getAll(User user) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return offeredServiceRepository.getAll();
    }

    @Override
    public OfferedService getById(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return offeredServiceRepository.getById(id);
    }

    @Override
    public OfferedService getByField(User user, String name) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return offeredServiceRepository.getByField(name);
    }


    @Override
    public void create(User user, OfferedService offeredService) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        validateExisting(offeredService);
        offeredServiceRepository.create(offeredService);
    }

    @Override
    public void update(User user, OfferedService offeredService) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        validateUnique(offeredService);
        offeredServiceRepository.update(offeredService);
    }

    @Override
    public void delete(User user, int id) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        OfferedService existing = offeredServiceRepository.getById(id);
        existing.setEnabled(false);
        offeredServiceRepository.update(existing);
    }

    @Override
    public List<OfferedService> filter(User user, OfferedServiceFilterParameters ofp) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return offeredServiceRepository.filter(ofp);
    }

    @Override
    public String generateReportPDF(User user, Optional<String> currency, VisitFilterParameters vfp) {
        verifyUserHasRoles(user, UserRoles.CUSTOMER, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        if (user.hasRights()) {
            vfp.setUserId(null);
        }
        if (vfp.getVehicleId().isEmpty()) {
            throw new InvalidRequestParamsException("Select vehicle id.");
        }
        String report = "";

        double rate = getRate(currency);
        String abbreviatedCurrency = getAbbreviatedCurrency(currency);

        List<Visit> result = visitRepository.filter(rate, vfp);

        if (result.isEmpty()) {
            throw new EntityNotFoundException("No selected offered services.");
        }

        report = generateReportPDF(user, result, abbreviatedCurrency);

        return report;
    }

    private String generateReportPDF(User user, List<Visit> visits, String abbreviatedCurrency) {
        String toEmail = user.getEmail();
        mailSender.sendReportViaEmail(toEmail, visits, abbreviatedCurrency, "pdfReport.pdf");
//        mailSender.sendReportViaEmail("ivelinmurdzhev@gmail.com", visits, abbreviatedCurrency, "pdfReport.pdf");
        return "Sending email with report...";
    }

    private void validateExisting(OfferedService offeredService) {
        boolean duplicateExists = true;
        try {
            offeredServiceRepository.getByField(offeredService.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("OfferedService", "name", offeredService.getName());
        }
    }

    private void validateUnique(OfferedService offeredService) {
        boolean duplicateExists = true;
        try {
            OfferedService existing = offeredServiceRepository.getByField(offeredService.getName());
            if (existing.getId() == offeredService.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("OfferedService");
        }
    }

    private double getRate(Optional<String> currency) {
        if (currency.isPresent()) {
            CurrencyExchanger currencyExchanger = new CurrencyExchanger();
            return currencyExchanger.getRate(currency.get().toUpperCase(Locale.ROOT));
        } else return 1;
    }

    private String getAbbreviatedCurrency(Optional<String> currency) {
        return currency.orElse("BGN");
    }

}
