package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.exceptions.ExpiryTokenFailureException;
import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.dtos.ResetPasswordDto;
import com.telerikacademy.web.smartgarage.models.dtos.UserDto;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;
import com.telerikacademy.web.smartgarage.services.contracts.TokenService;
import com.telerikacademy.web.smartgarage.services.contracts.UserService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(tags = "Users")
@RestController
@Valid
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final ModelMapper modelMapper;
    private final TokenService tokenService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService,
                          ModelMapper modelMapper,
                          TokenService tokenService,
                          AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.tokenService = tokenService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Find all Users",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getAll(user);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find User by Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public User getById(@RequestHeader HttpHeaders headers,
                        @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getById(user, id);
    }

    @GetMapping("/email")
    @ApiOperation(value = "Find Users by email",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public User getByEmail(@RequestHeader HttpHeaders headers,
                           @RequestParam String email) {
        authenticationHelper.tryGetUser(headers);
        return userService.getByEmail(email);
    }

    @GetMapping("/first-name")
    @ApiOperation(value = "Find User by his first name",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public User getByFirstName(@RequestHeader HttpHeaders headers,
                               @RequestParam String firstName) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getByField(user, firstName);
    }

    @GetMapping("/filter")
    @ApiOperation(value = "Filter User by first name, last name, email," +
            " phone number, vehicle id, begin date or end date",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<User> filter(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> firstName,
                             @RequestParam(required = false) Optional<String> lastName,
                             @RequestParam(required = false) Optional<String> email,
                             @RequestParam(required = false) Optional<String> phoneNumber,
                             @RequestParam(required = false) Optional<Integer> vehicleModelId,
                             @RequestParam(required = false) Optional<String> beginDate,
                             @RequestParam(required = false) Optional<String> endDate) {
        User user = authenticationHelper.tryGetUser(headers);
        UserFilterParameters ufp = modelMapper.fromParametersList(
                firstName, lastName, email, phoneNumber, vehicleModelId, beginDate, endDate);
        return userService.filter(user, ufp);
    }

    @GetMapping("/sort")
    @ApiOperation(value = "Sort the Users be first name, begin date or end date " +
            "order by ascending or descending",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<User> sort(@RequestHeader HttpHeaders headers,
                           @RequestParam Optional<String> firstName,
                           @RequestParam Optional<String> beginDate,
                           @RequestParam Optional<String> endDate) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.sort(user, firstName, beginDate, endDate);
    }

    @PostMapping("/forgotten-password")
    @ApiOperation(value = "Reset User password by sending him email (if exists) with instructions and token for access.",
            notes = "Accessible from customer",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public String resetPassword(@RequestParam String email) {
        return userService.resetPassword(email);
    }

    @PutMapping("/updated-password")
    @ApiOperation(value = "Update User password by using the current token",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public String updatePassword(@RequestParam("token") String token,
                                 @Valid @RequestBody ResetPasswordDto dto) {
        try {
            tokenService.isExpired(token);
            return userService.resetPasswordConfirm(token, dto.getEmail(), dto.getPassword(), dto.getConfirmPassword());
        } catch (ExpiryTokenFailureException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Create User and sending him an email with generated password.",
            notes = "Only accessible from admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public User create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody UserDto userDto) {
        User authenticationUser = authenticationHelper.tryGetUser(headers);
        User user = modelMapper.fromDto(userDto);
        userService.create(authenticationUser, user);
        return user;
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update User by given Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public User update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id,
                       @Valid @RequestBody UserDto userDto) {
        User authenticationUser = authenticationHelper.tryGetUser(headers);
        User user = modelMapper.fromDto(userDto, id);
        userService.update(authenticationUser, user);
        return user;
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete User by given Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        userService.delete(user, id);
    }

}
