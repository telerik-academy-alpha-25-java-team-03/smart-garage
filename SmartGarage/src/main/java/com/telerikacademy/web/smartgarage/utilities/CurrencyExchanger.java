package com.telerikacademy.web.smartgarage.utilities;

import com.posadskiy.currencyconverter.CurrencyConverter;
import com.posadskiy.currencyconverter.config.ConfigBuilder;

public class CurrencyExchanger {

    private static final String CURRENCY_CONVERTER_API_API_KEY = "f586503131522afc9e1a";
    private final CurrencyConverter currencyConverter;

    public CurrencyExchanger() {
        this.currencyConverter = new CurrencyConverter(
                new ConfigBuilder()
                        .currencyConverterApiApiKey(CURRENCY_CONVERTER_API_API_KEY)
                        .build()
        );
    }

    public double getRate(String rate) {
        return currencyConverter.rate("BGN", rate);
    }

}
