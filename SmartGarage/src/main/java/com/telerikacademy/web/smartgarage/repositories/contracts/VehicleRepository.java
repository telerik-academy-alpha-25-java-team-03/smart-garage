package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.filterParameters.VehicleFilterParameters;

import java.util.List;

public interface VehicleRepository extends CRURepositoryOperations<Vehicle> {

    Vehicle getByVin(String vin);

    List<Vehicle> filterByOwner(VehicleFilterParameters vfp);

    void disableVehicles(int userId);

}
