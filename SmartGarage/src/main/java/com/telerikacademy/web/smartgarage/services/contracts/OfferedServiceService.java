package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.filterParameters.OfferedServiceFilterParameters;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;

import java.util.List;
import java.util.Optional;

public interface OfferedServiceService extends CRUDServiceOperations<OfferedService> {

    List<OfferedService> filter(User user, OfferedServiceFilterParameters ofp);

    String generateReportPDF(User user, Optional<String> currency, VisitFilterParameters vfp);
}
