package com.telerikacademy.web.smartgarage.utilities;

import com.telerikacademy.web.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.UnauthorizedOperationException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice("com.telerikacademy.web.smartgarage.controllers.mvc")
public class MVCControllerExceptionHandler {

    @ExceptionHandler(value = EntityNotFoundException.class)
    protected String handleConflict(EntityNotFoundException exception, Model model) {
        model.addAttribute("message", exception.getMessage());
        return "not-found";
    }

    @ExceptionHandler(value = DuplicateEntityException.class)
    protected String handleConflict(DuplicateEntityException exception, Model model) {
        model.addAttribute("message", exception.getMessage());
        return "errorpages409";
    }

    @ExceptionHandler(value = UnauthorizedOperationException.class)
    protected String handleConflict(UnauthorizedOperationException exception, Model model) {
        model.addAttribute("currentUser", null);
        model.addAttribute("message", exception.getMessage());
        return "unauthorized";
    }

    @ExceptionHandler(value = AuthenticationFailureException.class)
    protected String handleConflict(AuthenticationFailureException exception, Model model) {
        model.addAttribute("message", exception.getMessage());
        return "errorpages401";
    }

}
