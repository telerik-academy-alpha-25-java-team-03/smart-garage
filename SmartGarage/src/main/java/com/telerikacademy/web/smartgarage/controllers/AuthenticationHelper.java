package com.telerikacademy.web.smartgarage.controllers;

import com.telerikacademy.web.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.services.contracts.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String LOGIN_FAILURE_MESSAGE = "N0 logged in user";
    private static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong email/password!";
    private static final String EMAIL_NOT_EXISTING = "Wrong email!";

    private final UserService userService;

    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");
        if (currentUser == null) {
            throw new UnauthorizedOperationException(LOGIN_FAILURE_MESSAGE);
        }
        return userService.getByEmail(currentUser);
    }

    public User verifyAuthentication(String email, String password) {
        try {
            User user = userService.getByEmail(email);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }

    public User verifyAuthentication(String email) {
        try {
            User user = userService.getByEmail(email);
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(EMAIL_NOT_EXISTING);
        }
    }

    public User tryGetUser(HttpHeaders headers) {
        verifyAuthorization(headers);
        String userEmail = headers.getFirst(AUTHORIZATION_HEADER_NAME);
        return userService.getByEmail(userEmail);
    }

    private void verifyAuthorization(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "The requested resource requires authentication!");
        }
    }

}
