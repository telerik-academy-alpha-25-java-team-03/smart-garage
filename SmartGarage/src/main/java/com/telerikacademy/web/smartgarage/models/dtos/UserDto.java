package com.telerikacademy.web.smartgarage.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class UserDto {

    @NotNull(message = "First name cannot be empty!")
    @Size(min = 2, max = 20, message = "First name must be between 2 and 20 symbols!")
    private String firstName;

    @NotNull(message = "Last name cannot be empty!")
    @Size(min = 2, max = 20, message = "Last name must be between 2 and 20 symbols!")
    private String lastName;

    @Size(min = 10, max = 10, message = "Phone number must be 10 digits!")
    private String phoneNumber;

    @Email
    private String email;

    private Set<String> roles;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

}
