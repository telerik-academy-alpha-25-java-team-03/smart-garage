package com.telerikacademy.web.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;

public class OfferedServiceDto {

    @NotNull
    private String name;

    @NotNull
    private Double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
