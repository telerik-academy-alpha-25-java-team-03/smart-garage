package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.filterParameters.VehicleFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class VehicleRepositoryImpl
        extends AbstractGenericCRUDRepository<Vehicle> implements VehicleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Vehicle> getAll() {
        return super.getAll(Vehicle.class);
    }

    @Override
    public Vehicle getById(int id) {
        return super.getByField("id", id, Vehicle.class);
    }

    @Override
    public Vehicle getByField(String registrationPlate) {
        return super.getByField("registrationPlate", registrationPlate, Vehicle.class);
    }

    @Override
    public Vehicle getByVin(String vin) {
        return super.getByField("vin", vin, Vehicle.class);
    }

    @Override
    public void create(Vehicle vehicleToCreate) {
        super.create(vehicleToCreate);
    }

    @Override
    public void update(Vehicle vehicleToUpdate) {
        super.update(vehicleToUpdate);
    }

    @Override
    public List<Vehicle> filterByOwner(VehicleFilterParameters vfp) {
        try (Session session = sessionFactory.openSession()) {
            String baseQuery = " from Vehicle ";
            var filters = new ArrayList<String>();

            vfp.getFirstName().ifPresent(first -> filters.add(" user.firstName like concat('%', :firstName , '%')"));
            vfp.getLastName().ifPresent(last -> filters.add(" user.lastName like concat('%', :lastName , '%')"));
            vfp.getEmail().ifPresent(first -> filters.add(" user.email like concat('%', :email , '%')"));
            if (!filters.isEmpty()) {
                baseQuery = baseQuery + " where " + String.join(" and ", filters);
            }
            Query<Vehicle> query = session.createQuery(baseQuery, Vehicle.class);
            vfp.getFirstName().ifPresent(first -> query.setParameter("firstName", first));
            vfp.getLastName().ifPresent(last -> query.setParameter("lastName", last));
            vfp.getEmail().ifPresent(email -> query.setParameter("email", email));

            List<Vehicle> result = query.list();
            if (result.size() == 0) {
                StringBuilder notFoundElements = new StringBuilder();
                notFoundElements.append("Vehicle attributes:");
                if (vfp.getFirstName().isPresent())
                    notFoundElements.append(" First name: [").append(vfp.getFirstName().get()).append("]");
                if (vfp.getLastName().isPresent())
                    notFoundElements.append(" Last name: [").append(vfp.getLastName().get()).append("]");
                if (vfp.getEmail().isPresent())
                    notFoundElements.append(" Email: [").append(vfp.getEmail().get()).append("]");
                throw new EntityNotFoundException(notFoundElements.toString());
            }
            return query.getResultList();
        }
    }

    @Override
    public void disableVehicles(int userId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            String disableQuery = " update Vehicle set enabled = false where user.id = :userId ";

            var query = session.createQuery(disableQuery).
                    setParameter("userId", userId).
                    executeUpdate();
            session.getTransaction().commit();
        }
    }

}
