package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.dtos.OfferedServiceDto;
import com.telerikacademy.web.smartgarage.models.dtos.VisitFilterParamsDto;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;
import com.telerikacademy.web.smartgarage.services.contracts.OfferedServiceService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
@RequestMapping("/services")
public class OfferedServiceMvcController {

    private final OfferedServiceService offeredServiceService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public OfferedServiceMvcController(OfferedServiceService offeredServiceService,
                                       ModelMapper modelMapper,
                                       AuthenticationHelper authenticationHelper) {
        this.offeredServiceService = offeredServiceService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showAllOfferedServices(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        List<OfferedService> services = offeredServiceService.getAll(user);
        model.addAttribute("services", services);
        model.addAttribute("currentUser", user);
        return "services";
    }

    @GetMapping("/create")
    public String showNew(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        model.addAttribute("offeredServiceDto", new OfferedServiceDto());
        return "service-create";
    }

    @PostMapping("/create")
    public String handleNew(@Valid @ModelAttribute("OfferedServiceDto") OfferedServiceDto offeredServiceDto,
                            HttpSession session,
                            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "service-create";
        }
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            OfferedService offeredService = modelMapper.fromDto(offeredServiceDto);
            offeredServiceService.create(currentUser, offeredService);
        } catch (DuplicateEntityException exception) {
            bindingResult.rejectValue("name", "name", exception.getMessage());
            return "service-create";
        }
        return "redirect:/services";
    }

    @GetMapping("/{id}/update")
    public String showEdit(@PathVariable int id, Model model, HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        OfferedService offeredService = offeredServiceService.getById(currentUser, id);
        model.addAttribute("offeredService", offeredService);
        OfferedServiceDto offeredServiceDto = modelMapper.toOfferedServiceDto(offeredService);
        model.addAttribute("offeredServiceDto", offeredServiceDto);
        return "service-update";
    }

    @PostMapping("/{id}/update")
    public String handleEdit(@PathVariable int id,
                             @Valid @ModelAttribute("offeredServiceDto") OfferedServiceDto offeredServiceDto,
                             HttpSession session,
                             BindingResult errors) {
        if (errors.hasErrors()) {
            return "service-update";
        }
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            OfferedService offeredService = modelMapper.fromDto(offeredServiceDto, id);
            offeredServiceService.update(currentUser, offeredService);
            return "redirect:/services";
        } catch (EntityNotFoundException exception) {
            errors.rejectValue("name", "name", exception.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String handleDelete(HttpSession session,
                               @PathVariable int id) {
        User currentUser = authenticationHelper.tryGetUser(session);
        OfferedService offeredService = offeredServiceService.getById(currentUser, id);
        offeredServiceService.delete(currentUser, id);
        return "redirect:/services";
    }

    @GetMapping("/report")
    public String emailReport(Model model) {
        model.addAttribute("vfp", new VisitFilterParamsDto());
        return "reports";
    }


    @PostMapping("/report")
    public String handleEmailReport(HttpSession session,
                                    @ModelAttribute("vfp") VisitFilterParamsDto vfpDto) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            VisitFilterParameters vfp = modelMapper.fromDto(vfpDto);
            offeredServiceService.generateReportPDF(user, vfpDto.getCurrency(), vfp);
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
        return "services";
    }

    @PostMapping("/download")
    public void downloadFile(HttpSession session,
                             @ModelAttribute("vfp") VisitFilterParamsDto dto,
                             HttpServletResponse response) throws IOException {

        User user = authenticationHelper.tryGetUser(session);
        VisitFilterParameters vfp = modelMapper.fromDto(dto);
        offeredServiceService.generateReportPDF(user, dto.getCurrency(), vfp);
        Path fileLocation = Paths.get("smart-garage/SmartGarage/pdfReport.pdf");

        byte[] byteArray = Files.readAllBytes(fileLocation);
        response.setContentType(MimeTypeUtils.APPLICATION_OCTET_STREAM.getType());
        response.setHeader("Download Report", "PDF");
        response.setContentLength(byteArray.length);

        try (OutputStream outputStream = response.getOutputStream()) {
            outputStream.write(byteArray, 0, byteArray.length);
        }
    }
    
}
