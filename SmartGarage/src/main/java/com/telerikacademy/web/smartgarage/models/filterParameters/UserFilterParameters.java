package com.telerikacademy.web.smartgarage.models.filterParameters;

import java.util.Optional;

public class UserFilterParameters {

    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> email;
    private Optional<String> phoneNumber;
    private Optional<Integer> vehicleModelId;
    private Optional<String> beginDate;
    private Optional<String> endDate;

    public Optional<String> getFirstName() {
        return firstName;
    }

    public UserFilterParameters setFirstName(Optional<String> firstName) {
        this.firstName = firstName;
        return this;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public UserFilterParameters setLastName(Optional<String> lastName) {
        this.lastName = lastName;
        return this;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public UserFilterParameters setEmail(Optional<String> email) {
        this.email = email;
        return this;
    }

    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }

    public UserFilterParameters setPhoneNumber(Optional<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public Optional<Integer> getVehicleModelId() {
        return vehicleModelId;
    }

    public UserFilterParameters setVehicleModelId(Optional<Integer> vehicleModelId) {
        this.vehicleModelId = vehicleModelId;
        return this;
    }

    public Optional<String> getBeginDate() {
        return beginDate;
    }

    public UserFilterParameters setBeginDate(Optional<String> beginDate) {
        this.beginDate = beginDate;
        return this;
    }

    public Optional<String> getEndDate() {
        return endDate;
    }

    public UserFilterParameters setEndDate(Optional<String> endDate) {
        this.endDate = endDate;
        return this;
    }
}
