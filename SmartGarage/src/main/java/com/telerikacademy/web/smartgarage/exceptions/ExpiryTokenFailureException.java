package com.telerikacademy.web.smartgarage.exceptions;

public class ExpiryTokenFailureException extends RuntimeException {

    public ExpiryTokenFailureException(String message) {
        super(message);
    }
}
