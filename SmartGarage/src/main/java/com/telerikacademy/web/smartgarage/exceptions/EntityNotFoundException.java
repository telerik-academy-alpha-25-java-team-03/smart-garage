package com.telerikacademy.web.smartgarage.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found!", type, attribute, value));
    }

    public EntityNotFoundException(String name) {
        super(String.format("%s not found!", name));
    }

}
