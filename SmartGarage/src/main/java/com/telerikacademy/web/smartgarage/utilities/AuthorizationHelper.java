package com.telerikacademy.web.smartgarage.utilities;

import com.telerikacademy.web.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.enums.UserRoles;

import java.util.Arrays;
import java.util.stream.Collectors;

public class AuthorizationHelper {

    private static final String NOT_HAVE_THE_REQUIRED_AUTHORIZATION =
            "User does not have the required authorization!";

    public static void verifyUserHasRoles(User user, UserRoles... wantedRoles) {
        var wantedRolesSet = Arrays.stream(wantedRoles)
                .map(UserRoles::toString)
                .collect(Collectors.toSet());

        user.getRoles()
                .stream()
                .map(role -> role.getType().toLowerCase())
                .filter(wantedRolesSet::contains)
                .findAny()
                .orElseThrow(() -> new UnauthorizedOperationException(NOT_HAVE_THE_REQUIRED_AUTHORIZATION));
    }
}
