package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public abstract class AbstractGenericGetRepository<T> {

    private final SessionFactory sessionFactory;

    public AbstractGenericGetRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<T> getAll(Class<T> clazz) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(String.format("from %s where enabled=true", clazz.getName()), clazz).list();
        }
    }

    // id, email, name
    public <V> T getByField(String fieldName, V value, Class<T> clazz) {
        try (Session session = sessionFactory.openSession()) {
            String like = " like concat('%', :value , '%')";
            String queryString = String.format("from %s where enabled=true and %s %s", clazz.getName(), fieldName, like);
            Query<T> query = session.createQuery(queryString, clazz);
            query.setParameter("value", value);
            List<T> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(
                        String.format("%s with %s %s",
                                clazz.getSimpleName(),
                                fieldName,
                                value));
            }
            return result.get(0);
        }
    }

}
