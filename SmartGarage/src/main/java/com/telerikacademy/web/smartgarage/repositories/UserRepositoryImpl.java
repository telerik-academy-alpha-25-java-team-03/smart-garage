package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.InvalidRequestParamsException;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.filterParameters.UserFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl
        extends AbstractGenericCRUDRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        return super.getAll(User.class);
    }

    @Override
    public User getById(int id) {
        return super.getByField("id", id, User.class);
    }

    @Override
    public User getByField(String firstName) {
        return super.getByField("firstName", firstName, User.class);
    }

    @Override
    public void create(User user) {
        super.create(user);
    }

    @Override
    public void update(User userToUpdate) {
        super.update(userToUpdate);
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return users.get(0);
        }
    }

    @Override
    public List<User> filter(UserFilterParameters ufp) {
        try (Session session = sessionFactory.openSession()) {
            String baseQuery =
                    "select u from User u " +
                            "join Vehicle ve on ve.user=u " +
                            "join Visit vi on vi.vehicle=ve";

            var filters = new ArrayList<String>();
            ufp.getEmail().ifPresent(e -> filters.add(" u.email like concat('%', :email , '%') "));
            ufp.getFirstName().ifPresent(f -> filters.add(" u.firstName like concat('%', :firstName , '%') "));
            ufp.getLastName().ifPresent(l -> filters.add(" u.lastName like concat('%', :lastName , '%') "));
            ufp.getPhoneNumber().ifPresent(p -> filters.add(" u.phoneNumber like concat('%', :phoneNumber , '%') "));
            ufp.getVehicleModelId().ifPresent(v -> filters.add(" ve.model.id = :vehicleModelId "));
            ufp.getBeginDate().ifPresent(bd -> filters.add(" vi.beginDate >= :beginDate "));
            ufp.getEndDate().ifPresent(ed -> filters.add(" vi.endDate <= :endDate "));


            if (!filters.isEmpty()) {
                baseQuery = baseQuery + " where u.enabled=true and " + String.join(" and ", filters);
            }

            Query<User> query = session.createQuery(baseQuery, User.class);
            ufp.getEmail().ifPresent(email -> query.setParameter("email", email));
            ufp.getFirstName().ifPresent(first -> query.setParameter("firstName", first));
            ufp.getLastName().ifPresent(last -> query.setParameter("lastName", last));
            ufp.getPhoneNumber().ifPresent(phone -> query.setParameter("phoneNumber", phone));
            ufp.getVehicleModelId().ifPresent(model -> query.setParameter("vehicleModelId", model));
            ufp.getBeginDate().ifPresent(begin -> query.setParameter("beginDate", LocalDate.parse(begin)));
            ufp.getEndDate().ifPresent(end -> query.setParameter("endDate", LocalDate.parse(end)));

            List<User> result = query.list();
            if (result.size() == 0) {
                String notFound = "User with attributes ";
                if (ufp.getFirstName().isPresent()) notFound += "[First Name]: '" + ufp.getFirstName().get() + "'";
                if (ufp.getLastName().isPresent()) notFound += " " + "[Last Name]: '" + ufp.getLastName().get() + "'";
                if (ufp.getEmail().isPresent()) notFound += " " + "[Email]: '" + ufp.getEmail().get() + "'";
                if (ufp.getPhoneNumber().isPresent())
                    notFound += " " + "[Phone Number]: '" + ufp.getPhoneNumber().get() + "'";
                if (ufp.getVehicleModelId().isPresent())
                    notFound += " " + "[Vehicle Model ID]: '" + ufp.getVehicleModelId().get() + "'";
                if (ufp.getBeginDate().isPresent())
                    notFound += " " + "[Begin Date]: '" + ufp.getBeginDate().get() + "'";
                if (ufp.getEndDate().isPresent()) notFound += " " + "[End Date]: '" + ufp.getEndDate().get() + "'";

                throw new EntityNotFoundException(notFound);
            }
            return result;
        }
    }

    @Override
    public List<User> sort(Optional<String> firstName,
                           Optional<String> beginDate,
                           Optional<String> endDate) {
        boolean firstNameIsPresent = firstName.isPresent();
        boolean beginDateIsPresent = beginDate.isPresent();
        boolean endDateIsPresent = endDate.isPresent();
        String baseQuery =
                "select distinct u from User u " +
                        "join Vehicle ve on ve.user=u " +
                        "join Visit vi on vi.vehicle=ve " +
                        "where u.enabled=true " +
                        "order by ";
        if (firstNameIsPresent) {
            baseQuery += "u.firstName " + firstName.get();
        }
        if (beginDateIsPresent) {
            baseQuery += "vi.beginDate " + beginDate.get();
        }
        if (endDateIsPresent) {
            baseQuery += "vi.endDate " + endDate.get();
        }
        if (beginDateIsPresent && endDateIsPresent) {
            throw new InvalidRequestParamsException("Choose between beginDate or endDate carefully!");
        }
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(baseQuery, User.class);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User");
            }
            return result;
        }
    }

}
