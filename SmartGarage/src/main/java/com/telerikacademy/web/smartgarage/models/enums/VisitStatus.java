package com.telerikacademy.web.smartgarage.models.enums;

public enum VisitStatus {
    NOT_STARTED,
    IN_PROGRESS,
    READY_FOR_PICKUP;

    @Override
    public String toString() {
        switch (this) {
            case NOT_STARTED:
                return "Not Started";
            case IN_PROGRESS:
                return "In Progress";
            case READY_FOR_PICKUP:
                return "Ready for Pickup";
            default:
                throw new IllegalArgumentException("Unavailable");
        }
    }
}
