package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.User;

public interface ManufacturerService extends ReadServiceOperations<Manufacturer> {

    void create(User user, Manufacturer manufacturer);

}
