package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Model;

public interface ModelRepository extends ReadRepositoryOperations<Model> {

    void create(Model model);

}
