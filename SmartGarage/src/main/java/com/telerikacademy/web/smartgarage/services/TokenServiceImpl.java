package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.models.PasswordResetToken;
import com.telerikacademy.web.smartgarage.repositories.contracts.TokenRepository;
import com.telerikacademy.web.smartgarage.services.contracts.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {

    private final TokenRepository tokenRepository;

    @Autowired
    public TokenServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public PasswordResetToken createToken() {
        return tokenRepository.createToken();
    }

    @Override
    public boolean isExpired(String tokenText) {
        return tokenRepository.isExpired(tokenText);
    }

    @Override
    public PasswordResetToken getToken(String tokenText) {
        return tokenRepository.getToken(tokenText);
    }

    @Override
    public void markTokenAsAccessed(String tokenText) {
        tokenRepository.markTokenAsAccessed(tokenText);
    }

}
