package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.PasswordResetToken;

public interface TokenRepository {

    PasswordResetToken createToken();

    boolean isExpired(String tokenText);

    PasswordResetToken getToken(String tokenText);

    void markTokenAsAccessed(String tokenText);
}
