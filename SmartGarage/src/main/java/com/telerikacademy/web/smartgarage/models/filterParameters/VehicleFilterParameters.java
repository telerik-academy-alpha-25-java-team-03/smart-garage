package com.telerikacademy.web.smartgarage.models.filterParameters;

import java.util.Optional;

public class VehicleFilterParameters {

    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> email;

    public Optional<String> getFirstName() {
        return firstName;
    }

    public VehicleFilterParameters setFirstName(Optional<String> firstName) {
        this.firstName = firstName;
        return this;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public VehicleFilterParameters setLastName(Optional<String> lastName) {
        this.lastName = lastName;
        return this;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public VehicleFilterParameters setEmail(Optional<String> email) {
        this.email = email;
        return this;
    }
}
