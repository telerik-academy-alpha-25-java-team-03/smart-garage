package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.PasswordResetToken;

public interface TokenService {

    PasswordResetToken createToken();

    boolean isExpired(String tokenText);

    PasswordResetToken getToken(String tokenText);

    void markTokenAsAccessed(String tokenText);

}
