package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.enums.UserRoles;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.web.smartgarage.services.contracts.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.web.smartgarage.utilities.AuthorizationHelper.verifyUserHasRoles;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public List<Manufacturer> getAll(User user) {
        return manufacturerRepository.getAll();
    }

    @Override
    public Manufacturer getById(User user, int id) {
        return manufacturerRepository.getById(id);
    }

    @Override
    public Manufacturer getByField(User user, String name) {
        return manufacturerRepository.getByField(name);
    }

    @Override
    public void create(User user, Manufacturer manufacturer) {
        verifyUserHasRoles(user, UserRoles.EMPLOYEE, UserRoles.ADMIN);
        try {
            manufacturerRepository.getByField(manufacturer.getName());
        } catch (EntityNotFoundException e) {
            manufacturerRepository.create(manufacturer);
        }
    }

}
