package com.telerikacademy.web.smartgarage.models.dtos;

import java.util.Optional;

public class VisitFilterParamsDto {

    private Integer userId;
    private Optional<Integer> vehicleId;
    private Optional<String> registrationPlate;
    private Optional<String> beginDate;
    private Optional<String> endDate;
    private Optional<String> currency;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Optional<Integer> getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Optional<Integer> vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Optional<String> getRegistrationPlate() {
        return registrationPlate;
    }

    public void setRegistrationPlate(Optional<String> registrationPlate) {
        this.registrationPlate = registrationPlate;
    }

    public Optional<String> getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Optional<String> beginDate) {
        this.beginDate = beginDate;
    }

    public Optional<String> getEndDate() {
        return endDate;
    }

    public void setEndDate(Optional<String> endDate) {
        this.endDate = endDate;
    }

    public Optional<String> getCurrency() {
        return currency;
    }

    public void setCurrency(Optional<String> currency) {
        this.currency = currency;
    }
}
