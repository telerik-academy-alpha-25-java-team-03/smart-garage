package com.telerikacademy.web.smartgarage.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "password_reset_token")
public class PasswordResetToken {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonIgnore
    @NotNull(message = "Cannot be null")
    @Column(name = "token")
    private String token;

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiry_time")
    private Date expiryTime;

    @JsonIgnore
    @Column(name = "accessed")
    private boolean accessed;

    public PasswordResetToken() {
        setExpiryTime();
        setToken();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public boolean isAccessed() {
        return accessed;
    }

    public void setAccessed(boolean accessed) {
        this.accessed = accessed;
    }

    public Date setExpiryTime() {
        Date date = new Date();
        Date creationTime = new Timestamp(date.getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(creationTime);
        calendar.add(Calendar.HOUR, 1);
        this.expiryTime = calendar.getTime();
        return calendar.getTime();
    }

    public String setToken() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] bytes = new byte[20];
        secureRandom.nextBytes(bytes);
        this.token = removeChars(bytes);
        return token;
    }

    private String removeChars(byte[] bytes) {
        return Arrays.toString(bytes)
                .replaceAll("[-/,]", "")
                .replaceAll(" ", "")
                .substring(1, bytes.length - 1);
    }
}
