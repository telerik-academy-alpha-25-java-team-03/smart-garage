package com.telerikacademy.web.smartgarage.repositories.contracts;

public interface CRURepositoryOperations<T> extends ReadRepositoryOperations<T> {

    void create(T object);

    void update(T object);

}
