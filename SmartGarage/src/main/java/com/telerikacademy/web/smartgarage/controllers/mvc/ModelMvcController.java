package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.dtos.ModelDto;
import com.telerikacademy.web.smartgarage.services.contracts.ManufacturerService;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/models")
public class ModelMvcController {

    private final ModelService modelService;
    private final ManufacturerService manufacturerService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ModelMvcController(ModelService modelService,
                              ManufacturerService manufacturerService,
                              ModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.modelService = modelService;
        this.manufacturerService = manufacturerService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/create")
    public String create(org.springframework.ui.Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("modelDto", new ModelDto());
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
        return "model-create";
    }

    @PostMapping("/create")
    public String handleCreate(@Valid @ModelAttribute("modelDto") ModelDto dto,
                               HttpSession session, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "model-create";
        }
        try {
            User admin = authenticationHelper.tryGetUser(session);
            Manufacturer manufacturer = modelMapper.fromDto(dto.getManufacturerDto());
            manufacturerService.create(admin, manufacturer);
            Model model = modelMapper.fromDto(dto);
            modelService.create(admin, model);
        }catch (UnauthorizedOperationException e) {
            return "unauthorized";
        } catch (DuplicateEntityException d) {
            bindingResult.rejectValue("name", "duplicate_model_name", d.getMessage());
            return "model-create";
        }
        return "redirect:/vehicles/create";
    }
}
