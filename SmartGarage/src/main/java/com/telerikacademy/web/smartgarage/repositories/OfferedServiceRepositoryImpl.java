package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.filterParameters.OfferedServiceFilterParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.OfferedServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OfferedServiceRepositoryImpl
        extends AbstractGenericCRUDRepository<OfferedService> implements OfferedServiceRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public OfferedServiceRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<OfferedService> getAll() {
        return super.getAll(OfferedService.class);
    }

    @Override
    public OfferedService getById(int id) {
        return super.getByField("id", id, OfferedService.class);
    }

    @Override
    public OfferedService getByField(String name) {
        return super.getByField("name", name, OfferedService.class);
    }

    @Override
    public void create(OfferedService offeredService) {
        super.create(offeredService);
    }

    @Override
    public void update(OfferedService offeredService) {
        super.update(offeredService);
    }

    @Override
    public List<OfferedService> filter(OfferedServiceFilterParameters ofp) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = " from OfferedService ";
            var filters = new ArrayList<String>();

            ofp.getName().ifPresent(n -> filters.add(" name like concat('%', :name , '%') "));
            ofp.getMinPrice().ifPresent(min -> filters.add(" price > :minPrice "));
            ofp.getMaxPrice().ifPresent(max -> filters.add(" price < :maxPrice "));
            if (!filters.isEmpty()) {
                baseQuery = baseQuery + " where " + String.join(" and ", filters);
            }
            Query<OfferedService> query = session.createQuery(baseQuery, OfferedService.class);
            ofp.getName().ifPresent(name -> query.setParameter("name", name));
            ofp.getMinPrice().ifPresent(min -> query.setParameter("minPrice", min));
            ofp.getMaxPrice().ifPresent(max -> query.setParameter("maxPrice", max));

            var result = query.list();
            if (result.size() == 0) {
                String notFound = "Attribute with";
                if (ofp.getName().isPresent()) notFound += " Name: [" + ofp.getName().get() + "]";
                if (ofp.getMinPrice().isPresent()) notFound += " Min Price: [" + ofp.getMinPrice().get() + "]";
                if (ofp.getMaxPrice().isPresent()) notFound += " Max Price: [" + ofp.getMaxPrice().get() + "]";

                throw new EntityNotFoundException(notFound);
            }
            return query.list();
        }
    }

}
