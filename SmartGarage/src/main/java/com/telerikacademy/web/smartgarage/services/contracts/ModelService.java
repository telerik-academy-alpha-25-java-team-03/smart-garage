package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.User;

public interface ModelService extends ReadServiceOperations<Model> {

    void create(User user, Model model);

}
