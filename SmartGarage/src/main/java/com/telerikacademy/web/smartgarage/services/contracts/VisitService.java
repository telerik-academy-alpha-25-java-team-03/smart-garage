package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;

import java.util.List;
import java.util.Optional;

public interface VisitService {

    List<Visit> getAll(User user);

    Visit getById(User user, int id);

    void create(User user, Visit visit);

    void update(User user, Visit visit);

    void delete(User user, int id);

    List<Visit> sort(User user, Optional<String> beginDate, Optional<String> endDate);

    List<Visit> filter(User user, Optional<String> currency, VisitFilterParameters vfp);

}
