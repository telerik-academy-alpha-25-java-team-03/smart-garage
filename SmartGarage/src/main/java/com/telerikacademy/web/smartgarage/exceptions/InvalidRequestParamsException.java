package com.telerikacademy.web.smartgarage.exceptions;

public class InvalidRequestParamsException extends RuntimeException {

    public InvalidRequestParamsException(String message) {
        super(message);
    }
}
