package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelRepositoryImpl
        extends AbstractGenericGetRepository<Model> implements ModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Model> getAll() {
        return super.getAll(Model.class);
    }

    @Override
    public Model getById(int id) {
        return super.getByField("id", id, Model.class);
    }

    @Override
    public Model getByField(String name) {
        return super.getByField("name", name, Model.class);
    }

    @Override
    public void create(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(model);
            session.getTransaction().commit();
        }
    }

}
