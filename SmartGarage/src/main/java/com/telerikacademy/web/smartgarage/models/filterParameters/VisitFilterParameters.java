package com.telerikacademy.web.smartgarage.models.filterParameters;

import java.util.Optional;

public class VisitFilterParameters {

    private Integer userId;
    private Optional<Integer> vehicleId;
    private Optional<String> registrationPlate;
    private Optional<String> beginDate;
    private Optional<String> endDate;

    public Integer getUserId() {
        return userId;
    }

    public VisitFilterParameters setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public Optional<Integer> getVehicleId() {
        return vehicleId;
    }

    public VisitFilterParameters setVehicleId(Optional<Integer> vehicleId) {
        this.vehicleId = vehicleId;
        return this;
    }

    public Optional<String> getRegistrationPlate() {
        return registrationPlate;
    }

    public VisitFilterParameters setRegistrationPlate(Optional<String> registrationPlate) {
        this.registrationPlate = registrationPlate;
        return this;
    }

    public Optional<String> getBeginDate() {
        return beginDate;
    }

    public VisitFilterParameters setBeginDate(Optional<String> fromDate) {
        this.beginDate = fromDate;
        return this;
    }

    public Optional<String> getEndDate() {
        return endDate;
    }

    public VisitFilterParameters setEndDate(Optional<String> endDate) {
        this.endDate = endDate;
        return this;
    }
}
