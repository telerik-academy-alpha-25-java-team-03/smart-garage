package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.filterParameters.VehicleFilterParameters;

import java.util.List;

public interface VehicleService extends CRUDServiceOperations<Vehicle> {

    Vehicle getByVin(User user, String vin);

    List<Vehicle> filterByOwner(User user, VehicleFilterParameters vfp);

}
