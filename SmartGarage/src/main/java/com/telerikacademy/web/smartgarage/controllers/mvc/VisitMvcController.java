package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.smartgarage.models.OfferedService;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.dtos.VisitDto;
import com.telerikacademy.web.smartgarage.services.contracts.OfferedServiceService;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    private final VisitService visitService;
    private final VehicleService vehicleService;
    private final OfferedServiceService offeredService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VisitMvcController(VisitService visitService,
                              VehicleService vehicleService,
                              OfferedServiceService offeredService, ModelMapper modelMapper,
                              AuthenticationHelper authenticationHelper) {
        this.visitService = visitService;
        this.vehicleService = vehicleService;
        this.offeredService = offeredService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("vehicles")
    public List<Vehicle> populateVehicles(HttpSession session) {
        return vehicleService.getAll(authenticationHelper.tryGetUser(session));
    }

    @ModelAttribute("services")
    public List<OfferedService> populateOfferedServices(HttpSession session) {
        return offeredService.getAll(authenticationHelper.tryGetUser(session));
    }

    @GetMapping
    public String showAllVisits(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", user);
            List<Visit> visits = visitService.getAll(user);
            model.addAttribute("visits", visits);
            return "visits";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
    }

    @GetMapping("/create")
    public String create(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("visitDto", new VisitDto());
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
        return "visit-create";
    }

    @PostMapping("/create")
    public String handleCreate(@Valid @ModelAttribute("visitDto") VisitDto dto,
                               HttpSession session, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "visit-create";
        }
        try {
            User admin = authenticationHelper.tryGetUser(session);
            Visit newVisit = modelMapper.fromDto(dto);
            visitService.create(admin, newVisit);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        } catch (DuplicateEntityException d) {
            bindingResult.rejectValue("name", "duplicate_name", d.getMessage());
        }
        return "redirect:/visits";
    }

    @GetMapping("/{id}/update")
    public String showEditVehiclePage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User admin = authenticationHelper.tryGetUser(session);

            Visit visitToUpdate = visitService.getById(admin, id);
            model.addAttribute("visitToUpdate", visitToUpdate);

            VisitDto visitDto = modelMapper.toVisitDto(visitToUpdate);
            model.addAttribute("visitDto", visitDto);
            return "visit-update";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        } catch (EntityNotFoundException d) {
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateVisit(@PathVariable int id,
                              @Valid @ModelAttribute("visitDto") VisitDto dto,
                              HttpSession session, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "vehicle-update";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            Visit visit = modelMapper.fromDto(dto, id);
            visitService.update(user, visit);
            return "redirect:/visits";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
//        catch (EntityNotFoundException e) {
//            bindingResult.rejectValue("visitDto.date", "date.exists", e.getMessage());
//            return "not-found";
//        }
    }

    @GetMapping("/{id}/delete")
    public String handleDeleteVisit(@PathVariable int id, Model model, HttpSession session) {
        try {
            User admin = authenticationHelper.tryGetUser(session);
            Visit visit = visitService.getById(admin, id);
            visitService.delete(admin, id);
            return "redirect:/visits";
        }catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }
}
