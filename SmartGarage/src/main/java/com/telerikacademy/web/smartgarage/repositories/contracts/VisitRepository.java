package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.filterParameters.VisitFilterParameters;

import java.util.List;
import java.util.Optional;

public interface VisitRepository {

    List<Visit> getAll();

    Visit getById(int id);

    void create(Visit visit);

    void update(Visit visit);

    List<Visit> sort(Optional<String> beginDate, Optional<String> endDate);

    List<Visit> filter(double rate, VisitFilterParameters vfp);

    void disableVisits(int vehicleId);

}
