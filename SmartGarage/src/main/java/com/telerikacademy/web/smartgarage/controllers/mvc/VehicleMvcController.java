package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.dtos.VehicleDto;
import com.telerikacademy.web.smartgarage.services.contracts.ManufacturerService;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/vehicles")
public class VehicleMvcController {

    private final VehicleService vehicleService;
    private final ModelService modelService;
    private final ManufacturerService manufacturerService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VehicleMvcController(VehicleService vehicleService,
                                ModelService modelService,
                                ManufacturerService manufacturerService, ModelMapper modelMapper,
                                AuthenticationHelper authenticationHelper) {
        this.vehicleService = vehicleService;
        this.modelService = modelService;
        this.manufacturerService = manufacturerService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("models")
    public List<Model> populateModels(HttpSession session) {
        return modelService.getAll(authenticationHelper.tryGetUser(session));
    }

    @ModelAttribute("manufacturers")
    public List<Manufacturer> populateManufacturers(HttpSession session) {
        return manufacturerService.getAll(authenticationHelper.tryGetUser(session));
    }

    @GetMapping
    public String showAllVehicles(org.springframework.ui.Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", user);
            List<Vehicle> vehicles = vehicleService.getAll(user);
            model.addAttribute("vehicles", vehicles);
            return "vehicles";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
    }

    @GetMapping("/create")
    public String create(org.springframework.ui.Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("vehicleDto", new VehicleDto());
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
        return "vehicle-create";
    }

    @PostMapping("/create")
    public String handleCreate(@Valid @ModelAttribute("vehicleDto") VehicleDto dto,
                               HttpSession session, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "vehicle-create";
        }
        try {
            User admin = authenticationHelper.tryGetUser(session);
            Vehicle newVehicle = modelMapper.fromDto(dto);
            vehicleService.create(admin, newVehicle);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        } catch (DuplicateEntityException d) {
            bindingResult.rejectValue("VIN", "duplicate_vin", d.getMessage());
            bindingResult.rejectValue("registrationPlate", "duplicate_registrationPlate", d.getMessage());
            return "vehicle-create";
        }
        return "redirect:/vehicles";
    }

    @GetMapping("/{id}/update")
    public String showEditVehiclePage(@PathVariable int id,
                                      org.springframework.ui.Model model,
                                      HttpSession session) {
        try {
            User admin = authenticationHelper.tryGetUser(session);

            Vehicle vehicleToUpdate = vehicleService.getById(admin, id);
            model.addAttribute("vehicleToUpdate", vehicleToUpdate);

            VehicleDto vehicleDto = modelMapper.toVehicleDto(vehicleToUpdate);
            model.addAttribute("vehicleDto", vehicleDto);

            return "vehicle-update";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        } catch (EntityNotFoundException d) {
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateVehicle(@PathVariable int id,
                                @Valid @ModelAttribute("vehicleDto") VehicleDto vehicleDto,
                                HttpSession session,
                                BindingResult errors) {
        if (errors.hasErrors()) {
            return "vehicle-update";
        }

        try {
            User admin = authenticationHelper.tryGetUser(session);
            Vehicle vehicle = modelMapper.fromDto(vehicleDto, id);
            vehicleService.update(admin, vehicle);
            return "redirect:/vehicles";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("vehicleDto.registrationPLate", "registrationPLate.exists", e.getMessage());
            errors.rejectValue("vehicleDto.vin", "vin.exists", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
    }

    @GetMapping("/{id}/delete")
    public String handleDeleteVehicle(org.springframework.ui.Model model,
                                      HttpSession session,
                                      @PathVariable int id) {
        try {
            User admin = authenticationHelper.tryGetUser(session);
            Vehicle vehicle = vehicleService.getById(admin, id);
            vehicleService.delete(admin, id);
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }

}
