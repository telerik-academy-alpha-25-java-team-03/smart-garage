package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.dtos.VehicleDto;
import com.telerikacademy.web.smartgarage.models.filterParameters.VehicleFilterParameters;
import com.telerikacademy.web.smartgarage.services.contracts.ManufacturerService;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import com.telerikacademy.web.smartgarage.utilities.ModelMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(tags = "Vehicles")
@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final ManufacturerService manufacturerService;
    private final ModelService modelService;
    private final VehicleService vehicleService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VehicleController(ManufacturerService manufacturerService,
                             ModelService modelService,
                             VehicleService vehicleService,
                             ModelMapper modelMapper,
                             AuthenticationHelper authenticationHelper) {
        this.manufacturerService = manufacturerService;
        this.modelService = modelService;
        this.vehicleService = vehicleService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Find all Vehicles",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<Vehicle> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return vehicleService.getAll(user);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find Vehicle by Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public Vehicle getById(@RequestHeader HttpHeaders headers,
                           @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return vehicleService.getById(user, id);
    }

    @GetMapping("/registration-plate")
    @ApiOperation(value = "Find Vehicle by registration plate",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public Vehicle getByRegistrationPlate(@RequestHeader HttpHeaders headers,
                                          @RequestParam String registrationPlate) {
        User user = authenticationHelper.tryGetUser(headers);
        return vehicleService.getByField(user, registrationPlate);
    }

    @GetMapping("/vin")
    @ApiOperation(value = "Find Vehicle by VIN",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public Vehicle getByVin(@RequestHeader HttpHeaders headers,
                            @RequestParam String vin) {
        User user = authenticationHelper.tryGetUser(headers);
        return vehicleService.getByVin(user, vin);
    }

    @GetMapping("/filter")
    @ApiOperation(value = "Filter Vehicle by Owner's first name, last name, email",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public List<Vehicle> filterByOwner(@RequestHeader HttpHeaders headers,
                                       @RequestParam(required = false) Optional<String> firstName,
                                       @RequestParam(required = false) Optional<String> lastName,
                                       @RequestParam(required = false) Optional<String> email) {
        User user = authenticationHelper.tryGetUser(headers);
        VehicleFilterParameters vfp = modelMapper.fromVehicleParametersList(firstName, lastName, email);
        return vehicleService.filterByOwner(user, vfp);
    }

    @PostMapping
    @ApiOperation(value = "Create a new Vehicle",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public Vehicle create(@RequestHeader HttpHeaders headers,
                          @Valid @RequestBody VehicleDto vehicleDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Manufacturer manufacturer = modelMapper.fromDto(vehicleDto.getModelDto().getManufacturerDto());
        manufacturerService.create(user, manufacturer);
        Model model = modelMapper.fromDto(vehicleDto.getModelDto());
        modelService.create(user, model);
        Vehicle vehicle = modelMapper.fromDto(vehicleDto);
        vehicleService.create(user, vehicle);
        return vehicle;
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Vehicle by given Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public Vehicle update(@RequestHeader HttpHeaders headers,
                          @PathVariable int id,
                          @Valid @RequestBody VehicleDto vehicleDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Vehicle vehicle = modelMapper.fromDto(vehicleDto, id);
        vehicleService.update(user, vehicle);
        return vehicle;
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Vehicle by given Id",
            notes = "Only accessible from employees and admins",
            response = OfferedService.class)
    @ApiImplicitParam(name = "Authorization",
            dataTypeClass = HttpHeaders.class,
            required = true,
            paramType = "header",
            example = "Ivan123",
            value = "See example below")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        vehicleService.delete(user, id);
    }

}
