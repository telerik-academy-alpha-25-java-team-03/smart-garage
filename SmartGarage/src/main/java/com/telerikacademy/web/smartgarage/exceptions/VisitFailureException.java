package com.telerikacademy.web.smartgarage.exceptions;

public class VisitFailureException extends RuntimeException {

    public VisitFailureException(String message) {
        super(message);
    }
}
