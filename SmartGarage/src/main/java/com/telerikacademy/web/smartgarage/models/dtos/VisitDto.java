package com.telerikacademy.web.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class VisitDto {

    @NotNull(message = "Begin date cannot be empty!")
    private String beginDate;

    @NotNull(message = "End date cannot be empty!")
    private String endDate;

    @Positive
    private int vehicleId;

    private String offeredServicesIds;

    public VisitDto() {
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getOfferedServicesIds() {
        return offeredServicesIds;
    }

    public void setOfferedServicesIds(String offeredServicesIds) {
        this.offeredServicesIds = offeredServicesIds;
    }

}
