create
database if not exists `smart_garage`;
use
`smart_garage`;

create table manufacturers
(
    id      int auto_increment
        primary key,
    name    varchar(30) not null,
    enabled tinyint(1) default 1 not null,
    constraint manufacturers_name_uindex
        unique (name)

);

create table models
(
    id              int auto_increment
        primary key,
    name            varchar(30) not null,
    manufacturer_id int         not null,
    enabled         tinyint(1) default 1 not null,
    constraint models_name_uindex
        unique (name),
    constraint models_manufacturers_manufacturer_id_fk
        foreign key (manufacturer_id) references manufacturers (id)
);

create table roles
(
    id   int auto_increment
        primary key,
    type varchar(30) not null
);

create table services
(
    id      int auto_increment
        primary key,
    name    varchar(50) not null,
    price   double      not null,
    enabled tinyint(1) default 1 not null,
    constraint services_name_uindex
        unique (name)
);

create table users
(
    id           int auto_increment
        primary key,
    first_name   varchar(20) not null,
    last_name    varchar(20) not null,
    phone_number varchar(10) not null,
    email        varchar(50) not null,
    password     varchar(30) not null,
    enabled      tinyint(1) default 1 not null,
    constraint users_email_uindex
        unique (email),
    constraint users_phone_number_uindex
        unique (phone_number)
);

create table users_roles
(
    user_id int null,
    role_id int null,
    constraint users_roles_roles_role_id_fk
        foreign key (role_id) references roles (id),
    constraint users_roles_users_user_id_fk
        foreign key (user_id) references users (id)
);

create table vehicles
(
    id                 int auto_increment
        primary key,
    registration_plate varchar(10) not null,
    vin                varchar(17) not null,
    year               int         not null,
    model_id           int         not null,
    user_id            int         not null,
    enabled            tinyint(1) default 1 not null,
    constraint vehicles_registration_plate_uindex
        unique (registration_plate),
    constraint vehicles_vin_uindex
        unique (vin),
    constraint vehicles_models_model_id_fk
        foreign key (model_id) references models (id),
    constraint vehicles_users_user_id_fk
        foreign key (user_id) references users (id)
);

create table visits
(
    id          int auto_increment
        primary key,
    begin_date  date        not null,
    end_date    date        not null,
    status      varchar(20) not null,
    total_price double null,
    vehicle_id  int         not null,
    enabled     tinyint(1) default 1 not null,
    constraint visits_vehicles_vehicle_id_fk
        foreign key (vehicle_id) references vehicles (id)
);

create table visits_services
(
    visit_id   int not null,
    service_id int not null,
    constraint visits_services_services_service_id_fk
        foreign key (service_id) references services (id),
    constraint visits_services_visits_visit_id_fk
        foreign key (visit_id) references visits (id)
);

create table password_reset_token
(
    id          int auto_increment
        primary key,
    token       varchar(60) not null,
    expiry_time datetime    not null,
    accessed    tinyint(1) default 0 not null
);