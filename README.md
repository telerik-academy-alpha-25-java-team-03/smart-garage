# **SmartGarage**








# _Project Description_

SmartGarage is a web application which serves the needs of an auto repair shop. Shop owners as well as its visitors can make use of its services and have their car services easily monitored through the provided functionalities.


# _Link to Trello Board_

The following Trello board([link](https://trello.com/b/r2YQ9pCU/smartgarage)) represents the development process of the application in the team. It gives idea of the different steps taken to reach the required functionality, as well as a timeframe of when a given feature was implemented. The board also outlines any bugs faced during the preparation and any ideas for future improvements of the project, too.


# _Link to Swagger Documentation_

A Swagger documentation([link](http://localhost:8080/swagger-ui/)) holds the information that is required to facilitate the communication with the project API. The application must be run prior to accessing the above link.


# _Create and Fill Database with Data_

In order to develop a database which serves the needs of the application, one has to access the provided '**_create.sql_**' script in the '**_db_**' folder of the main directory and then execute it. After completion, the '**_insert.sql_**' script in the same directory can be executed, so that the database is filled with mock data.


# _Images of Database Relations_

A MindMeister mindmap which represents the different structures of the project can be seen here([link](https://www.mindmeister.com/1840739757)). A generated visualisation of the database can be seen upon following this([link](https://imgur.com/a/zpAFkhb)).


# _Build and Run Your Own Instance of the App_

To run their own instance of the app, one has to open the project folder with IntelliJ IDEA. Next, the '**_build.gradle_**' tool should be accessed and its funcion of reloading dependencies must be invoked. Then the project can be started by running '**_SmartgarageApplication.java_**' at the '**_src/main/java/com/telerikacademy/web/smartgarage_**' directory. After this, the user can proceed and use a REST API access tool of their choice or instead go on and open the provided Swagger documentation.


# _Images of the Project_

Visualisation of different application components as well as front-end design and architecture could be found in the following folder([link](https://gitlab.com/telerik-academy-alpha-25-java-team-03/smart-garage/-/tree/master/Project%20Pictures)).


# _Project Main Features_

The front-end of the application can be access on the following ([link](http://localhost:8080/)) once it has been started. MVC takes care of its functionality.
There is a parallelly developed RESTful API, too. It covers CRUD operations for main project models and adds filter and sort capabilities to their database entries.
Customers of the shop are also able to see generated PDF report containing details about a concrete visit which is sent to them via email(they can set a currency of their choice). They are able to invoke a forgotten password key as well, which allows them to access their profile in case of emergencies.
